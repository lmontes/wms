﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WMS.DTO;
using WMS.Vistas;

namespace WMS.DAO
{
    class OrdenDAO
    {
        public ArticuloDTO ConsultarOrden(SqlConnection conn, OrdenDTO dao)
        {
            SqlCommand cmd = new SqlCommand("sp_wms_rcprar_dsno", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@p_orgen", dao._strOrigen);
            cmd.Parameters.AddWithValue("@p_nmro_orden", dao._strClientDBProductionOrderId);
            cmd.Parameters.AddWithValue("@p_WarehouseCode", Globales.gs_cdgoBodega);
            cmd.Connection.Open();

            SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);

            ArticuloDTO dto = new ArticuloDTO();

            while (dr.Read())
            {
                dto._lngCdgoArtclo = Convert.ToString(dr["cdgo_dsno"]);
                dto._strNmbreArtclo = Convert.ToString(dr["nmbre_dsno"]);
                dto._strAncho = Convert.ToString(dr["cdgo_ancho"]);
                dto._strLrgo = Convert.ToString(dr["lrgo_ttal"]);
                dto._strClor = Convert.ToString(dr["nmbre_clor"]);
                dto._strNmbreSubLneaInv = Convert.ToString(dr["nmbre_sblnea"]);
                dto._strNmbreCldad = Convert.ToString(dr["nmbre_cldad"]);
                dto._dblCntdadProducida = Convert.ToDouble(dr["cntdad_prdccion"]);
                dto._strProductionOrderId = Convert.ToString(dr["wms_productionorderid"]);
            }
            dr.Close();
            
            return dto;            
        }


        public ClienteDTO RecuperarCliente(SqlConnection conn, String ls_Origen, String ls_Parametro)
        {
            SqlCommand cmd = new SqlCommand("sp_wms_rcprar_clnte", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@p_orgen", ls_Origen);
            cmd.Parameters.AddWithValue("@p_prmtro", ls_Parametro);            
            cmd.Parameters.AddWithValue("@p_warehouseCode", Globales.gs_cdgoBodega);
            cmd.Connection.Open();

            SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);

            ClienteDTO dto = new ClienteDTO();

            while (dr.Read())
            {
                dto._strCodigo = Convert.ToString(dr["cdgo_clnte"]);
                dto._strRzonScial = Convert.ToString(dr["rzon_scial"]);
            }
            dr.Close();

            return dto;
        }

        public ArticuloDTO ObtenerInformacionDeLaOrdenPorUnitContainer(SqlConnection conn, ContenedorDTO contenedorDTO)
        {
            SqlCommand cmd = new SqlCommand("sp_ProductionOrder_GetInformation_DW_sps", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@p_UnitContainerBarcode", contenedorDTO._strCdgoBarrasPDF417);
            cmd.Connection.Open();

            SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);

            ArticuloDTO articuloDTO = new ArticuloDTO();

            while (dr.Read())
            {
                articuloDTO._strFuente = Convert.ToString(dr["documentsource"]);
                
                if (articuloDTO._strFuente.Equals("1"))
                {
                    articuloDTO._strClientDBProductionOrderId = Convert.ToString(dr["clientdbproductionorderid"]);
                }
                else
                {
                    articuloDTO._strProductionOrderId = Convert.ToString(dr["productionorderid"]);
                }

                articuloDTO._strEstado = Convert.ToString(dr["productionorderstatusvl"]);
                articuloDTO._lngCdgoArtclo = Convert.ToString(dr["productcode"]);
                articuloDTO._dblCntdadProducida = Convert.ToDouble(dr["quantitywithdecimals"]);
            }

            dr.Close();

            return articuloDTO;
        }

        public OrdenDTO ObtenerIdEstadoDeLaOrdenPorUnitContainer(SqlConnection conn, ContenedorDTO contenedorDTO)
        {
            SqlCommand cmd = new SqlCommand("sp_ProductionOrder_GetIdandStatus_DW_sps", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@p_UnitContainerBarcode", contenedorDTO._strCdgoBarrasPDF417);
            cmd.Connection.Open();

            SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);

            OrdenDTO articuloDTO = new OrdenDTO();

            while (dr.Read())
            {
                articuloDTO._strProductionOrderId = Convert.ToString(dr["productionorderid"]);
                articuloDTO._strClientDBProductionOrderId = Convert.ToString(dr["clientdbproductionorderid"]);
                articuloDTO._strEstado = Convert.ToString(dr["productionorderstatusvl"]);
                articuloDTO._strFuente = Convert.ToString(dr["documentsource"]);
                articuloDTO._dblCntdadRed = Convert.ToDouble(dr["productionitemstotalordered"]);
                articuloDTO._dblCntdadProducida = Convert.ToDouble(dr["productionitemstotalproduced"]);
            }

            dr.Close();

            return articuloDTO;
        }
    }
}
