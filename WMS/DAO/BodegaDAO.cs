﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WMS.DTO;

namespace WMS.DAO
{
    class BodegaDAO
    {
        public DataTable listarBodegasDelUsuario(SqlConnection conn, BodegaDTO dao)
        {
            SqlCommand cmd = new SqlCommand("sp_Warehouse_by_User", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@p_UserName", dao._strLogin);
            cmd.Connection.Open();
            

            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                conn.Dispose();
                cmd.Dispose();
            }
        }

        public BodegaDTO ConsultarBodegaPorLocalizacion(SqlConnection conn, LocalizacionDTO dao)
        {
            SqlCommand cmd = new SqlCommand("Locations_sps", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@p_LocationBarcode", dao._strCdgoBarrasPDF417);
            cmd.Connection.Open();

            SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);

            BodegaDTO dto = new BodegaDTO();

            while (dr.Read())
            {
                dto._sCdgo = Convert.ToString(dr["WarehouseCode"]);
            }
            dr.Close();

            return dto;
        }
    }
}
