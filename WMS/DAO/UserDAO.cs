﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WMS.DTO;

namespace WMS.DAO
{
    class UserDAO
    {
        public UserDTO oper_UserValidate(SqlConnection conn, UserDTO dao)
        {
            SqlCommand cmd = new SqlCommand("p_WMS_usrio", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Oper", "UV");//User validate
            cmd.Parameters.AddWithValue("@Login", dao._strLogin);
            cmd.Parameters.AddWithValue("@Password", dao._strPassword);
            cmd.Connection.Open();

            SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);

            UserDTO dto = new UserDTO();

            while (dr.Read())
            {
                dto._strLogin = Convert.ToString(dr["LOGIN"]);
                dto._strNombre = Convert.ToString(dr["NAME"]);
            }
            dr.Close();

            return dto;
        }
    }
}
