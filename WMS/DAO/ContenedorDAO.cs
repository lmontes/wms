﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WMS.DTO;

namespace WMS.DAO
{
    class ContenedorDAO
    {
        public ContenedorDTO generarEstiquer(SqlConnection conn, ContenedorDTO contenedorDTO, OrdenDTO ordenDTO, Double ld_Cant)
        {
            SqlCommand cmd = new SqlCommand("sp_InventoryItemBarcodes_CreateNewUnitContainerLabels_Batch", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@p_NumberOfUnitContainers", ld_Cant);
            cmd.Parameters.AddWithValue("@p_ProductionOrderId", ordenDTO._strProductionOrderId);
            cmd.Parameters.AddWithValue("@p_UnitContainerQuantity", contenedorDTO._dblCantContenidas);
            cmd.Parameters.AddWithValue("@p_UnitContainerTypeVL", contenedorDTO._strTipo);
            cmd.Connection.Open();

            SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);

            ContenedorDTO dto = new ContenedorDTO();

            while (dr.Read())
            {
                dto._strBatchId = Convert.ToString(dr[0]);
                dto._strClientDBProductionOrderId = ordenDTO._strClientDBProductionOrderId;
            }
            dr.Close();

            return dto;
        }

        public DataTable imprimirEstiquer(SqlConnection conn, ContenedorDTO contenedorDTO, OrdenDTO ordenDTO)
        {
            SqlCommand cmd = new SqlCommand("sp_Container_BatchPrintUnitContainers_DW", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@p_ProdOrderId", ordenDTO._strProductionOrderId);
            cmd.Parameters.AddWithValue("@p_BatchId", contenedorDTO._strBatchId);

            try
            {
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                conn.Dispose();
                cmd.Dispose();
            }
        }

        public ContenedorDTO GenerarContenedorPrincipal(SqlConnection conn)
        {
            SqlCommand cmd = new SqlCommand("sp_Container_PrintBoxLabel_DW", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection.Open();

            SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);

            ContenedorDTO dto = new ContenedorDTO();

            while (dr.Read())
            {
                dto._strCdgoBarrasPDF417 = Convert.ToString(dr["ContainerBarcode"]);
                dto._strTipo = Convert.ToString(dr["ContainerType"]);
                dto._dtmFecha = Convert.ToDateTime(dr["PrintDate"]);
            }
            dr.Close();

            return dto;
        }
    }
}
