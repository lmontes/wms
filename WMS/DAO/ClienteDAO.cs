﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WMS.DTO;
using WMS.Vistas;

namespace WMS.DAO
{
    class ClienteDAO
    {
        public DataTable listarClientes(SqlConnection conn)
        {
            SqlCommand cmd = new SqlCommand("sp_wms_rcprar_clnte_lsta", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@p_WarehouseCode", Globales.gs_cdgoBodega);
            cmd.CommandTimeout = 9000000;

            try
            {
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                conn.Dispose();
                cmd.Dispose();
            }
        }
    }
}
