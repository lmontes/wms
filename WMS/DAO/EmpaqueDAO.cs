﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using WMS.DTO;

namespace WMS.DAO
{
    class EmpaqueDAO
    {
        public void Empacar(SqlConnection conn, ContenedorDTO contenedorDTO, String ls_loclzcion, String ls_cntndor, String ls_listaCntndres)
        {
            SqlCommand cmd = new SqlCommand("sp_Container_PackContainer", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@p_ParentNew_LocationBarcode", ls_loclzcion);
            cmd.Parameters.AddWithValue("@p_Parent_ContainerBarcode", ls_cntndor);
            cmd.Parameters.AddWithValue("@p_PacketIn", ls_listaCntndres);
            cmd.Parameters.AddWithValue("@p_ParentContainerWeight", contenedorDTO._dblPeso);
            cmd.Parameters.AddWithValue("@p_vrfcar_op_crrda", "1");
            cmd.Connection.Open();
            try
            {
                cmd.ExecuteNonQuery();
                MessageBox.Show("Contenedores empaqcados satisfactoriamente.", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);

            }
            catch (SqlException)
            {
                MessageBox.Show("Ocurrió un error al realizar el proceso de empaque.", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
            }
            finally
            {
                conn.Dispose();
                cmd.Dispose();
            }
        }

        public void Desempacar(SqlConnection conn, ContenedorDTO contenedorDTO, String ls_loclzcion, String ls_cntndor, String ls_listaCntndres)
        {
            SqlCommand cmd = new SqlCommand("sp_Container_UnPackContainer", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@p_ParentNew_LocationBarcode", ls_loclzcion);
            cmd.Parameters.AddWithValue("@p_Parent_ContainerBarcode", ls_cntndor);
            cmd.Parameters.AddWithValue("@p_PacketIn", ls_listaCntndres);
            cmd.Parameters.AddWithValue("@p_ParentContainerWeight", contenedorDTO._dblPeso);
            cmd.Parameters.AddWithValue("@p_vrfcar_op_crrda", "1");
            cmd.Connection.Open();
            try
            {
                cmd.ExecuteNonQuery();
                MessageBox.Show("Contenedores Desempacados satisfactoriamente.", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);

            }
            catch (SqlException)
            {
                MessageBox.Show("Ocurrió un error al realizar el proceso de Desempaque.", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
            }
            finally
            {
                conn.Dispose();
                cmd.Dispose();
            }
        }

        public void Mover(SqlConnection conn, ContenedorDTO contenedorDTO, String ls_loclzcion, String ls_cntndor, String ls_listaCntndres)
        {
            SqlCommand cmd = new SqlCommand("sp_Container_MovePackContainer", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@p_ParentNew_LocationBarcode", ls_loclzcion);
            cmd.Parameters.AddWithValue("@p_Parent_ContainerBarcode", ls_cntndor);
            cmd.Parameters.AddWithValue("@p_PacketIn", ls_listaCntndres);
            cmd.Parameters.AddWithValue("@p_ParentContainerWeight", contenedorDTO._dblPeso);
            cmd.Connection.Open();
            try
            {
                cmd.ExecuteNonQuery();
                MessageBox.Show("Contenedores movidos satisfactoriamente.", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);

            }
            catch (SqlException)
            {
                MessageBox.Show("Ocurrió un error al realizar el proceso de Mover contenedores.", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
            }
            finally
            {
                conn.Dispose();
                cmd.Dispose();
            }
        }

        public void DividirEmpaque(SqlConnection conn, String ls_cntndor, String ls_listaCntndres)
        {
            SqlCommand cmd = new SqlCommand("sp_Container_SplitUnitContainer", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@p_Old_UnitContainerBarcode", ls_cntndor);
            cmd.Parameters.AddWithValue("@p_New_UnitContainerBarcode_List", ls_listaCntndres);
            cmd.Connection.Open();
            try
            {
                cmd.ExecuteNonQuery();
                MessageBox.Show("Contenedores divididos satisfactoriamente.", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);

            }
            catch (SqlException)
            {
                MessageBox.Show("Ocurrió un error al realizar el proceso de empaque.", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
            }
            finally
            {
                conn.Dispose();
                cmd.Dispose();
            }
        }

        public DataTable DividirContenedor(SqlConnection conn, String ls_listaCntndres)
        {
            SqlCommand cmd = new SqlCommand("sp_Container_PrintsSplitContainerLabel_DW", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@p_ContainerBarcodeList", ls_listaCntndres);
            cmd.Connection.Open();

            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                conn.Dispose();
                cmd.Dispose();
            }
        }

        public DataTable EmpacarSobrante(SqlConnection conn, ContenedorDTO contenedorDTO)
        {
            SqlCommand cmd = new SqlCommand("sp_Container_PrintLastUnitContainerLabel_DW", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@p_StandardContainerBarcode", contenedorDTO._strCdgoBarrasPDF417);
            cmd.Parameters.AddWithValue("@p_OddQuantity", contenedorDTO._dblCntdadProducida);
            cmd.Connection.Open();

            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                conn.Dispose();
                cmd.Dispose();
            }
        }
    }
}
