﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using MobilePrinter;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Collections;
using System.Runtime;
using Microsoft.Win32;
using MobilePrinter.TSCWinCE;

namespace WMS.Vistas
{
    public partial class FrmReimprsionEtqta : Form
    {
        public FrmReimprsionEtqta()
        {
            InitializeComponent();
            ListarTiposEtiqueta();
        }

        public void ListarTiposEtiqueta()
        {
            DataTable dtTable = new DataTable();
            dtTable.Columns.Add("CODIGO", typeof(string));
            dtTable.Columns.Add("DESCRIPCION", typeof(string));
            dtTable.Rows.Add("L", "Localizacion");
            dtTable.Rows.Add("B", "Contenedor principal");
            dtTable.Rows.Add("S", "contenedor pequeño");

            cbo_TpoEtiqueta.DisplayMember = "DESCRIPCION";
            cbo_TpoEtiqueta.ValueMember = "CODIGO";
            cbo_TpoEtiqueta.DataSource = dtTable;
        }        

        private void Print()
        {
            TSCEthernet IP = new TSCEthernet();
            IP.openport("192.21.21.35", 9100);
            
            IP.sendcommand("<xpml><page quantity='0' pitch='38.9 mm'></xpml>SIZE 78.7 mm, 38.9 mm\n");
            IP.sendcommand("DIRECTION 0,0\n");
            IP.sendcommand("REFERENCE 0,0\n");
            IP.sendcommand("OFFSET 0 mm\n");
            IP.sendcommand("SET PEEL OFF\n");
            IP.sendcommand("SET CUTTER OFF\n");
            IP.sendcommand("<xpml></page></xpml><xpml><page quantity='1' pitch='38.9 mm'></xpml>SET TEAR ON\n");
            IP.sendcommand("CLS\n");
            IP.sendcommand("CODEPAGE 1252\n");
            IP.sendcommand("TEXT 767,396,\"0\",180,8,8,\"S\"\n");
            IP.sendcommand("TEXT 722,396,\"0\",180,8,8,\"--55ZQU\"\n");
            IP.sendcommand("TEXT 231,388,\"3\",180,1,1,\"Finotex\"\n");
            IP.sendcommand("TEXT 276,355,\"3\",180,1,1,\"02/05/2015\"\n");
            IP.sendcommand("PDF417 673,307,673,307,180,W4,H12,C3,T0,\"S-----COLONMA112000500.00--55ZQU\"\n");
            IP.sendcommand("PRINT 1,1\n");
            IP.sendcommand("<xpml></page></xpml><xpml><end/></xpml>\n");

            IP.closeport();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            Print();
        }
    }
}