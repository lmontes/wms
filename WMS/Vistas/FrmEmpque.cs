﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WMS.DTE;
using WMS.DTO;

namespace WMS.Vistas
{
    public partial class FrmEmpque : Form
    {
        DataTable dt;
        String ls_lclzcionLeida;
        String ls_cntndorLeida;
        String ls_tpoPrcso;//Variable que determina el tipo de proceso que va a realizar la ventana si es empacar o mover.

        public FrmEmpque(String ls_tpoPrcso)
        {
            InitializeComponent();

            this.ls_tpoPrcso = ls_tpoPrcso;

            ls_lclzcionLeida = "N";//Inicialmente la variable localizacion empieza en N es decir que no se ha leido la localizacion.
            ls_cntndorLeida = "N";//Inicialmente la variable contenedor empieza en N es decir que no se ha leido el contenedor.

            DataGridTableStyle tabStyle = new DataGridTableStyle();
            tabStyle.MappingName = "Person";

            DataGridTextBoxColumn col1 = new DataGridTextBoxColumn();
            col1.MappingName = "name";
            col1.HeaderText = "CODIGO DEL CONTENEDOR";
            col1.Width = 229;

            tabStyle.GridColumnStyles.Add(col1);

            this.DG_CONTENEDOR.TableStyles.Clear();
            this.DG_CONTENEDOR.TableStyles.Add(tabStyle);
            
            dt = new DataTable("Person");
            dt.Columns.Add("name");
            this.DG_CONTENEDOR.DataSource = dt;

            txtCdgobrras.Focus();

            if (ls_tpoPrcso.Equals("E"))
            {
                btnEmpacar.Visible = true;
                btnDesempacar.Visible = true;
                btnLimpiarEmpacar.Visible = true;
            }
            if (ls_tpoPrcso.Equals("M"))
            {
                btnMover.Visible = true;
                btnLimpiarMover.Visible = true;
                lblTitulo.Text = "Mover de Contenedor a otro";
            }
        }

        public Boolean mtdValidarSleevIngresado()
        {
            foreach (DataRow row in dt.Rows)
            {
                if (row[0].ToString().Equals(txtCdgobrras.Text))
                {
                    MessageBox.Show("El contenedor " + row[0].ToString() + " ya fue leido.", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                    txtCdgobrras.Text = "";
                    txtCdgobrras.Focus();
                    return false;
                }
            }

            return true;
        }

        public void AgregarCodigo()
        {
            String lsCdigo = txtCdgobrras.Text;
            if (lsCdigo.Length < 32 && lsCdigo.Length > 32)
            {
                MessageBox.Show("Codigo incorrecto.", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                return;
            }

            if (ls_lclzcionLeida.Equals("N"))//Si no se ha leido la localizacion verifica que el codigo que esta leyendo es la localizacion.
            {
                if (lsCdigo.Substring(0, 1).Equals("L"))
                {
                    txtCodLoclzcion.Text = lsCdigo;// asignamos el codigo en el campo localizacion.
                    ls_lclzcionLeida = "S";// Cambia el estado de la localizacion a S es decir que si fuel leida.
                    txtCdgobrras.Text = "";
                    txtCdgobrras.Focus();//Paramos el codigo nuevamente en el campo que lee el codigo de barras.
                    return;
                }
                else
                {
                    MessageBox.Show("Escanee el codigo de barra inicialmente", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                    txtCdgobrras.Text = "";
                    txtCdgobrras.Focus();
                    return;
                }
            }

            if (ls_lclzcionLeida.Equals("S") && ls_cntndorLeida.Equals("N"))//Si ya se leyo la localizacio y aun no se ha leido el contenedor verificamos que lo que esta leyendo sea un contenedor.
            {
                if (lsCdigo.Substring(0, 1).Equals("L"))
                {
                    txtCodLoclzcion.Text = lsCdigo;// asignamos el codigo en el campo localizacion.
                    ls_lclzcionLeida = "S";// Cambia el estado de la localizacion a S es decir que si fuel leida.
                    txtCdgobrras.Text = "";
                    txtCdgobrras.Focus();//Paramos el codigo nuevamente en el campo que lee el codigo de barras.
                    return;
                }
                else if (lsCdigo.Substring(0, 1).Equals("P") || lsCdigo.Substring(0, 1).Equals("B"))
                {
                    txtContenedor.Text = lsCdigo;// asignamos el codigo en el campo localizacion.
                    ls_cntndorLeida = "S";// Cambia el estado del contenedor a S es decir que si fuel leido.
                    txtCdgobrras.Text = "";
                    txtCdgobrras.Focus();//Paramos el codigo nuevamente en el campo que lee el codigo de barras.
                    return;
                }
                else
                {
                    MessageBox.Show("Escanee el código del contenedor nuevamente.", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                    txtCdgobrras.Text = "";
                    txtCdgobrras.Focus();
                    return;
                }
            }

            if (ls_lclzcionLeida.Equals("S") && ls_cntndorLeida.Equals("S"))//Si ya fue leida la localizacion y el contenedor proseguimos a leer las cajitas.
            {
                if (lsCdigo.Substring(0, 1).Equals("L"))
                {
                    txtCodLoclzcion.Text = lsCdigo;// asignamos el codigo en el campo localizacion.
                    ls_lclzcionLeida = "S";// Cambia el estado de la localizacion a S es decir que si fuel leida.
                    txtCdgobrras.Text = "";
                    txtCdgobrras.Focus();//Paramos el codigo nuevamente en el campo que lee el codigo de barras.
                    return;
                }

                if (txtContenedor.Text.Substring(0, 1).Equals("P"))
                {
                    if (lsCdigo.Substring(0, 1).Equals("B"))
                    {
                        LBL_MENSAJE.Text = (dt.Rows.Count + 1).ToString() + " cajitas agregadas";
                        dt.Rows.Add(txtCdgobrras.Text);
                        txtCdgobrras.Text = "";
                        txtCdgobrras.Focus();
                        return;
                    }
                    else
                    {
                        MessageBox.Show("Solo cajas pueden ser empacadas en un palet.", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                        txtCdgobrras.Text = "";
                        txtCdgobrras.Focus();
                        return;
                    }
                }

                if (txtContenedor.Text.Substring(0, 1).Equals("B"))
                {
                    if (lsCdigo.Substring(0, 1).Equals("S") || lsCdigo.Substring(0, 1).Equals("R") || lsCdigo.Substring(0, 1).Equals("C") || lsCdigo.Substring(0, 1).Equals("O"))
                    {
                        LBL_MENSAJE.Text = (dt.Rows.Count + 1).ToString() + " cajitas agregadas";
                        dt.Rows.Add(txtCdgobrras.Text);
                        btnEmpacar.Enabled = true;
                        btnDesempacar.Enabled = true;
                        btnMover.Enabled = true;
                        btnLimpiarMover.Enabled = true;
                        btnLimpiarEmpacar.Enabled = true;
                        txtCdgobrras.Text = "";
                        txtCdgobrras.Focus();
                        return;
                    }
                    else
                    {
                        MessageBox.Show("Las cajas solo pueden ser empacadas en un contenedor..", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                        txtCdgobrras.Text = "";
                        txtCdgobrras.Focus();
                        return;
                    }
                }
            }          
        }

        public void Empacar()
        {
            BodegaDTO bodegaDTO = new BodegaDTO();
            LocalizacionDTO localizacionDTO = new LocalizacionDTO();
            ContenedorDTO contenedorDTO = new ContenedorDTO();
             
            BodegaDTE bodegaDTE = new BodegaDTE();
            EmpaqueDTE empaqueDTE = new EmpaqueDTE();                  

            if (txtCodLoclzcion.Text.Equals("") || txtContenedor.Text.Equals(""))
            {
                MessageBox.Show("No se puede guardar sin una localización y un contenedor primario.", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                txtCdgobrras.Text = "";
                txtCdgobrras.Focus();
                return;
            }


            if (txtPeso.Text.Equals(""))
            {
                contenedorDTO._dblPeso = 0;
            }
            else
            {
                contenedorDTO._dblPeso = Double.Parse(txtPeso.Text);
            }

            localizacionDTO._strCdgoBarrasPDF417 = txtCodLoclzcion.Text;
            bodegaDTO = bodegaDTE.ConsultarBodegaPorLocalizacion(localizacionDTO);//Consulta la bodega por localización para saber si la localizacion es del pais que hace referencia a la base de datos a la cual esta conectada.

            String ls_cdgoBdga = bodegaDTO._sCdgo;

            if (!ls_cdgoBdga.Equals(Globales.gs_cdgoBodega))//Si el codigo de la bodega consultado por codigo de localizacion es igual al codigo de la bodega con el que se loqueo el usuario permite continuar con el preoceso de guardado de empaque.
            {
                MessageBox.Show("Ubicación pertenece a un almacen diferente..!", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                txtCdgobrras.Text = "";
                txtCdgobrras.Focus();
                return;
            }

            String ls_ListaCntndres = dt.Rows.Count + "|";
            foreach (DataRow row in dt.Rows)
            {
                ls_ListaCntndres = ls_ListaCntndres + row[0].ToString();
            }

            //contenedorDTO._slocalizacion = txtCodLoclzcion.Text;
            //contenedorDTO._sCntndrPpal = txtContenedor.Text;
            //contenedorDTO._sCntndoEmpque = sCntndoEmpque;


            empaqueDTE.Empacar(contenedorDTO, txtCodLoclzcion.Text, txtContenedor.Text, ls_ListaCntndres);
        }

        public void mtdDesempacar()
        {
            BodegaDTO bodegaDTO = new BodegaDTO();
            LocalizacionDTO localizacionDTO = new LocalizacionDTO();
            ContenedorDTO contenedorDTO = new ContenedorDTO();

            BodegaDTE bodegaDTE = new BodegaDTE();
            EmpaqueDTE empaqueDTE = new EmpaqueDTE();  

            if (txtCodLoclzcion.Text.Equals("") || txtContenedor.Text.Equals(""))
            {
                MessageBox.Show("No se puede guardar sin una localización y un contenedor primario.", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                txtCdgobrras.Text = "";
                txtCdgobrras.Focus();
                return;
            }


            if (txtPeso.Text.Equals(""))
            {
                contenedorDTO._dblPeso = 0;
            }
            else
            {
                contenedorDTO._dblPeso = Double.Parse(txtPeso.Text);
            }

            localizacionDTO._strCdgoBarrasPDF417 = txtCodLoclzcion.Text;
            bodegaDTO = bodegaDTE.ConsultarBodegaPorLocalizacion(localizacionDTO);//Consulta la bodega por localización para saber si la localizacion es del pais hace referencia a la base de datos a la cual esta conectada.

            String ls_cdgoBdga = bodegaDTO._sCdgo;

            if (!ls_cdgoBdga.Equals(Globales.gs_cdgoBodega))//Si el codigo de la bodega consultado por codigo de localizacion es igual al codigo de la bodega con el que se loqueo el usuario permite continuar con el preoceso de guardado de empaque.
            {
                MessageBox.Show("Ubicación pertenece a un almacen diferente..!", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                txtCdgobrras.Text = "";
                txtCdgobrras.Focus();
                return;
            }

            String ls_ListaCntndres = dt.Rows.Count + "|";
            foreach (DataRow row in dt.Rows)
            {
                ls_ListaCntndres = ls_ListaCntndres + row[0].ToString();                
            }

            empaqueDTE.Desempacar(contenedorDTO, txtCodLoclzcion.Text, txtContenedor.Text, ls_ListaCntndres);
        }

        public void mtdMover()
        {
            BodegaDTO bodegaDTO = new BodegaDTO();
            LocalizacionDTO localizacionDTO = new LocalizacionDTO();
            ContenedorDTO contenedorDTO = new ContenedorDTO();

            BodegaDTE bodegaDTE = new BodegaDTE();
            EmpaqueDTE empaqueDTE = new EmpaqueDTE();  

            if (txtCodLoclzcion.Text.Equals("") || txtContenedor.Text.Equals(""))
            {
                MessageBox.Show("No se puede guardar sin una localización y un contenedor primario.", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                txtCdgobrras.Text = "";
                txtCdgobrras.Focus();
                return;
            }

            if (txtPeso.Text.Equals(""))
            {
                contenedorDTO._dblPeso = 0;
            }
            else
            {
                contenedorDTO._dblPeso = Double.Parse(txtPeso.Text);
            }

            localizacionDTO._strCdgoBarrasPDF417 = txtCodLoclzcion.Text;
            bodegaDTO = bodegaDTE.ConsultarBodegaPorLocalizacion(localizacionDTO);//Consulta la bodega por localización para saber si la localizacion es del pais hace referencia a la base de datos a la cual esta conectada.

            String ls_cdgoBdga = bodegaDTO._sCdgo;

            if (!ls_cdgoBdga.Equals(Globales.gs_cdgoBodega))//Si el codigo de la bodega consultado por codigo de localizacion es igual al codigo de la bodega con el que se loqueo el usuario permite continuar con el preoceso de guardado de empaque.
            {
                MessageBox.Show("Ubicación pertenece a un almacen diferente..!", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                txtCdgobrras.Text = "";
                txtCdgobrras.Focus();
                return;
            }

            String ls_ListaCntndres = dt.Rows.Count + "|";
            foreach (DataRow row in dt.Rows)
            {
                ls_ListaCntndres = ls_ListaCntndres + row[0].ToString();
            }

            empaqueDTE.Mover(contenedorDTO, txtCodLoclzcion.Text, txtContenedor.Text, ls_ListaCntndres);
        }

        private void TXT_CODIGO_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)// si se dio un enter
            {
                if (mtdValidarSleevIngresado())
                {
                    AgregarCodigo();
                }
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            Empacar();
        }

        private void btnGenerarStiker_Click(object sender, EventArgs e)
        {
            mtdDesempacar();
        }

        private void btnMover_Click(object sender, EventArgs e)
        {
            mtdMover();
        }
    }
}