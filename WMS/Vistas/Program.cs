﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Forms;
using WMS.Vistas;

using System.Runtime.InteropServices;

namespace WMS
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [MTAThread]
        static void Main()
        {
            FrmLogin login = new FrmLogin();
            login.ShowDialog();

            if (login.getExists().Equals(true))
            {
                Application.Run(new FrmPrincipal());
            }
        }
    }
}