﻿namespace WMS.Vistas
{
    partial class FrmDvdirCntndor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlContenedor = new System.Windows.Forms.Panel();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.pnlBordeRojo = new System.Windows.Forms.Panel();
            this.txtEstdoOrden = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtCntdad = new System.Windows.Forms.TextBox();
            this.txtCdgoPrdcto = new System.Windows.Forms.TextBox();
            this.txtOrdenPrdccion = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtCntndor = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.btnDividir = new System.Windows.Forms.Button();
            this.btnLimpiar = new System.Windows.Forms.Button();
            this.DG_CONTENEDOR = new System.Windows.Forms.DataGrid();
            this.LBL_MENSAJE = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.txtCantDivdir = new System.Windows.Forms.TextBox();
            this.txtCantContenedor = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtCantUnidades = new System.Windows.Forms.TextBox();
            this.pnlContenedor.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlContenedor
            // 
            this.pnlContenedor.BackColor = System.Drawing.Color.DimGray;
            this.pnlContenedor.Controls.Add(this.lblTitulo);
            this.pnlContenedor.Controls.Add(this.pnlBordeRojo);
            this.pnlContenedor.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlContenedor.Location = new System.Drawing.Point(0, 0);
            this.pnlContenedor.Name = "pnlContenedor";
            this.pnlContenedor.Size = new System.Drawing.Size(238, 26);
            // 
            // lblTitulo
            // 
            this.lblTitulo.ForeColor = System.Drawing.Color.White;
            this.lblTitulo.Location = new System.Drawing.Point(3, 3);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(208, 20);
            this.lblTitulo.Text = "Dividir Empaques";
            // 
            // pnlBordeRojo
            // 
            this.pnlBordeRojo.BackColor = System.Drawing.Color.Brown;
            this.pnlBordeRojo.Location = new System.Drawing.Point(0, 24);
            this.pnlBordeRojo.Name = "pnlBordeRojo";
            this.pnlBordeRojo.Size = new System.Drawing.Size(238, 3);
            // 
            // txtEstdoOrden
            // 
            this.txtEstdoOrden.BackColor = System.Drawing.Color.Silver;
            this.txtEstdoOrden.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.txtEstdoOrden.Location = new System.Drawing.Point(64, 115);
            this.txtEstdoOrden.Name = "txtEstdoOrden";
            this.txtEstdoOrden.ReadOnly = true;
            this.txtEstdoOrden.Size = new System.Drawing.Size(61, 19);
            this.txtEstdoOrden.TabIndex = 29;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label3.Location = new System.Drawing.Point(3, 118);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(115, 16);
            this.label3.Text = "Estado OP";
            // 
            // txtCntdad
            // 
            this.txtCntdad.BackColor = System.Drawing.Color.Silver;
            this.txtCntdad.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.txtCntdad.ForeColor = System.Drawing.Color.Red;
            this.txtCntdad.Location = new System.Drawing.Point(178, 115);
            this.txtCntdad.Name = "txtCntdad";
            this.txtCntdad.ReadOnly = true;
            this.txtCntdad.Size = new System.Drawing.Size(57, 19);
            this.txtCntdad.TabIndex = 30;
            // 
            // txtCdgoPrdcto
            // 
            this.txtCdgoPrdcto.BackColor = System.Drawing.Color.Silver;
            this.txtCdgoPrdcto.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.txtCdgoPrdcto.Location = new System.Drawing.Point(124, 96);
            this.txtCdgoPrdcto.Name = "txtCdgoPrdcto";
            this.txtCdgoPrdcto.ReadOnly = true;
            this.txtCdgoPrdcto.Size = new System.Drawing.Size(111, 19);
            this.txtCdgoPrdcto.TabIndex = 28;
            // 
            // txtOrdenPrdccion
            // 
            this.txtOrdenPrdccion.BackColor = System.Drawing.Color.Silver;
            this.txtOrdenPrdccion.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.txtOrdenPrdccion.Location = new System.Drawing.Point(124, 77);
            this.txtOrdenPrdccion.Name = "txtOrdenPrdccion";
            this.txtOrdenPrdccion.ReadOnly = true;
            this.txtOrdenPrdccion.Size = new System.Drawing.Size(111, 19);
            this.txtOrdenPrdccion.TabIndex = 27;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Tahoma", 8F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic)
                            | System.Drawing.FontStyle.Underline))));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label10.Location = new System.Drawing.Point(2, 61);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(232, 13);
            this.label10.Text = "Información de la orden de producción";
            // 
            // txtCntndor
            // 
            this.txtCntndor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtCntndor.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.txtCntndor.ForeColor = System.Drawing.Color.Black;
            this.txtCntndor.Location = new System.Drawing.Point(3, 42);
            this.txtCntndor.Name = "txtCntndor";
            this.txtCntndor.Size = new System.Drawing.Size(232, 19);
            this.txtCntndor.TabIndex = 26;
            this.txtCntndor.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCntndorAntiguo_KeyPress);
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label5.Location = new System.Drawing.Point(129, 119);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(86, 16);
            this.label5.Text = "Cantidad";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label4.Location = new System.Drawing.Point(3, 99);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(115, 16);
            this.label4.Text = "Código del producto";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label2.Location = new System.Drawing.Point(3, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(115, 16);
            this.label2.Text = "Orden de producción";
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(3, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(201, 16);
            this.label1.Text = "Código de el contenedor a dividir";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.button1.Location = new System.Drawing.Point(184, 175);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(50, 20);
            this.button1.TabIndex = 59;
            this.button1.Text = "Agregar";
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnDividir
            // 
            this.btnDividir.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.btnDividir.Location = new System.Drawing.Point(119, 247);
            this.btnDividir.Name = "btnDividir";
            this.btnDividir.Size = new System.Drawing.Size(70, 20);
            this.btnDividir.TabIndex = 57;
            this.btnDividir.Text = "Imprimir";
            this.btnDividir.Click += new System.EventHandler(this.btnDividir_Click);
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.btnLimpiar.Location = new System.Drawing.Point(49, 247);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(70, 20);
            this.btnLimpiar.TabIndex = 58;
            this.btnLimpiar.Text = "Limpiar";
            // 
            // DG_CONTENEDOR
            // 
            this.DG_CONTENEDOR.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.DG_CONTENEDOR.GridLineStyle = System.Windows.Forms.DataGridLineStyle.None;
            this.DG_CONTENEDOR.Location = new System.Drawing.Point(3, 174);
            this.DG_CONTENEDOR.Name = "DG_CONTENEDOR";
            this.DG_CONTENEDOR.RowHeadersVisible = false;
            this.DG_CONTENEDOR.Size = new System.Drawing.Size(179, 51);
            this.DG_CONTENEDOR.TabIndex = 56;
            // 
            // LBL_MENSAJE
            // 
            this.LBL_MENSAJE.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.LBL_MENSAJE.Location = new System.Drawing.Point(3, 140);
            this.LBL_MENSAJE.Name = "LBL_MENSAJE";
            this.LBL_MENSAJE.Size = new System.Drawing.Size(128, 16);
            this.LBL_MENSAJE.Text = "Nuevos contenedores";
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.button2.Location = new System.Drawing.Point(184, 199);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(50, 20);
            this.button2.TabIndex = 61;
            this.button2.Text = "Borrar";
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(2, 229);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(142, 16);
            this.label8.Text = "Total cantidad a dividir";
            // 
            // txtCantDivdir
            // 
            this.txtCantDivdir.BackColor = System.Drawing.Color.Silver;
            this.txtCantDivdir.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.txtCantDivdir.ForeColor = System.Drawing.Color.Red;
            this.txtCantDivdir.Location = new System.Drawing.Point(140, 227);
            this.txtCantDivdir.Name = "txtCantDivdir";
            this.txtCantDivdir.ReadOnly = true;
            this.txtCantDivdir.Size = new System.Drawing.Size(42, 19);
            this.txtCantDivdir.TabIndex = 70;
            // 
            // txtCantContenedor
            // 
            this.txtCantContenedor.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular);
            this.txtCantContenedor.ForeColor = System.Drawing.Color.Red;
            this.txtCantContenedor.Location = new System.Drawing.Point(96, 154);
            this.txtCantContenedor.Name = "txtCantContenedor";
            this.txtCantContenedor.Size = new System.Drawing.Size(28, 18);
            this.txtCantContenedor.TabIndex = 80;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label6.Location = new System.Drawing.Point(3, 157);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(87, 16);
            this.label6.Text = "Cant contenedor";
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label7.Location = new System.Drawing.Point(128, 157);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(51, 16);
            this.label7.Text = "Unidades";
            // 
            // txtCantUnidades
            // 
            this.txtCantUnidades.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular);
            this.txtCantUnidades.ForeColor = System.Drawing.Color.Red;
            this.txtCantUnidades.Location = new System.Drawing.Point(184, 154);
            this.txtCantUnidades.Name = "txtCantUnidades";
            this.txtCantUnidades.Size = new System.Drawing.Size(51, 18);
            this.txtCantUnidades.TabIndex = 85;
            // 
            // FrmDvdirCntndor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(238, 270);
            this.Controls.Add(this.txtCantUnidades);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtCantDivdir);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnDividir);
            this.Controls.Add(this.btnLimpiar);
            this.Controls.Add(this.DG_CONTENEDOR);
            this.Controls.Add(this.txtEstdoOrden);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtCntdad);
            this.Controls.Add(this.txtCdgoPrdcto);
            this.Controls.Add(this.txtOrdenPrdccion);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtCntndor);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pnlContenedor);
            this.Controls.Add(this.txtCantContenedor);
            this.Controls.Add(this.LBL_MENSAJE);
            this.Name = "FrmDvdirCntndor";
            this.Text = "Producto Terminado";
            this.pnlContenedor.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlContenedor;
        private System.Windows.Forms.Label lblTitulo;
        private System.Windows.Forms.Panel pnlBordeRojo;
        private System.Windows.Forms.TextBox txtEstdoOrden;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtCntdad;
        private System.Windows.Forms.TextBox txtCdgoPrdcto;
        private System.Windows.Forms.TextBox txtOrdenPrdccion;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtCntndor;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnDividir;
        private System.Windows.Forms.Button btnLimpiar;
        private System.Windows.Forms.Label LBL_MENSAJE;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtCantDivdir;
        private System.Windows.Forms.DataGrid DG_CONTENEDOR;
        private System.Windows.Forms.TextBox txtCantContenedor;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtCantUnidades;
    }
}