﻿namespace WMS.Vistas
{
    partial class FrmDvdirCntndoEmpque
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlContenedor = new System.Windows.Forms.Panel();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.pnlBordeRojo = new System.Windows.Forms.Panel();
            this.txtCntndorAntiguo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtOrdenPrdccion = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtCdgoPrdcto = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtEstdoOrden = new System.Windows.Forms.TextBox();
            this.txtCntdad = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtCntndorNuevo = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.LBL_MENSAJE = new System.Windows.Forms.Label();
            this.DG_CONTENEDOR = new System.Windows.Forms.DataGrid();
            this.dataGridTableStyle1 = new System.Windows.Forms.DataGridTableStyle();
            this.btnDividir = new System.Windows.Forms.Button();
            this.btnLimpiar = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.pnlContenedor.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlContenedor
            // 
            this.pnlContenedor.BackColor = System.Drawing.Color.DimGray;
            this.pnlContenedor.Controls.Add(this.lblTitulo);
            this.pnlContenedor.Controls.Add(this.pnlBordeRojo);
            this.pnlContenedor.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlContenedor.Location = new System.Drawing.Point(0, 0);
            this.pnlContenedor.Name = "pnlContenedor";
            this.pnlContenedor.Size = new System.Drawing.Size(238, 26);
            // 
            // lblTitulo
            // 
            this.lblTitulo.ForeColor = System.Drawing.Color.White;
            this.lblTitulo.Location = new System.Drawing.Point(3, 3);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(208, 20);
            this.lblTitulo.Text = "Dividir Empaques";
            // 
            // pnlBordeRojo
            // 
            this.pnlBordeRojo.BackColor = System.Drawing.Color.Brown;
            this.pnlBordeRojo.Location = new System.Drawing.Point(0, 24);
            this.pnlBordeRojo.Name = "pnlBordeRojo";
            this.pnlBordeRojo.Size = new System.Drawing.Size(238, 3);
            // 
            // txtCntndorAntiguo
            // 
            this.txtCntndorAntiguo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtCntndorAntiguo.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.txtCntndorAntiguo.ForeColor = System.Drawing.Color.Black;
            this.txtCntndorAntiguo.Location = new System.Drawing.Point(3, 41);
            this.txtCntndorAntiguo.Name = "txtCntndorAntiguo";
            this.txtCntndorAntiguo.Size = new System.Drawing.Size(232, 19);
            this.txtCntndorAntiguo.TabIndex = 6;
            this.txtCntndorAntiguo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCdgobrras_KeyPress);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(3, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(201, 16);
            this.label1.Text = "Código del antiguo contenedor";
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Tahoma", 8F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic)
                            | System.Drawing.FontStyle.Underline))));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label10.Location = new System.Drawing.Point(2, 61);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(232, 13);
            this.label10.Text = "Información de la orden de producción";
            // 
            // txtOrdenPrdccion
            // 
            this.txtOrdenPrdccion.BackColor = System.Drawing.Color.Silver;
            this.txtOrdenPrdccion.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.txtOrdenPrdccion.Location = new System.Drawing.Point(124, 76);
            this.txtOrdenPrdccion.Name = "txtOrdenPrdccion";
            this.txtOrdenPrdccion.ReadOnly = true;
            this.txtOrdenPrdccion.Size = new System.Drawing.Size(111, 19);
            this.txtOrdenPrdccion.TabIndex = 12;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label2.Location = new System.Drawing.Point(3, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(115, 16);
            this.label2.Text = "Orden de producción";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label3.Location = new System.Drawing.Point(3, 117);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(115, 16);
            this.label3.Text = "Estado OP";
            // 
            // txtCdgoPrdcto
            // 
            this.txtCdgoPrdcto.BackColor = System.Drawing.Color.Silver;
            this.txtCdgoPrdcto.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.txtCdgoPrdcto.Location = new System.Drawing.Point(124, 95);
            this.txtCdgoPrdcto.Name = "txtCdgoPrdcto";
            this.txtCdgoPrdcto.ReadOnly = true;
            this.txtCdgoPrdcto.Size = new System.Drawing.Size(111, 19);
            this.txtCdgoPrdcto.TabIndex = 16;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label4.Location = new System.Drawing.Point(3, 98);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(115, 16);
            this.label4.Text = "Código del producto";
            // 
            // txtEstdoOrden
            // 
            this.txtEstdoOrden.BackColor = System.Drawing.Color.Silver;
            this.txtEstdoOrden.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.txtEstdoOrden.Location = new System.Drawing.Point(64, 114);
            this.txtEstdoOrden.Name = "txtEstdoOrden";
            this.txtEstdoOrden.ReadOnly = true;
            this.txtEstdoOrden.Size = new System.Drawing.Size(61, 19);
            this.txtEstdoOrden.TabIndex = 19;
            // 
            // txtCntdad
            // 
            this.txtCntdad.BackColor = System.Drawing.Color.Silver;
            this.txtCntdad.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.txtCntdad.Location = new System.Drawing.Point(178, 114);
            this.txtCntdad.Name = "txtCntdad";
            this.txtCntdad.ReadOnly = true;
            this.txtCntdad.Size = new System.Drawing.Size(57, 19);
            this.txtCntdad.TabIndex = 20;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label5.Location = new System.Drawing.Point(129, 118);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(86, 16);
            this.label5.Text = "Cantidad";
            // 
            // txtCntndorNuevo
            // 
            this.txtCntndorNuevo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtCntndorNuevo.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.txtCntndorNuevo.ForeColor = System.Drawing.Color.Black;
            this.txtCntndorNuevo.Location = new System.Drawing.Point(3, 148);
            this.txtCntndorNuevo.Name = "txtCntndorNuevo";
            this.txtCntndorNuevo.Size = new System.Drawing.Size(232, 19);
            this.txtCntndorNuevo.TabIndex = 23;
            this.txtCntndorNuevo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox4_KeyPress);
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(3, 134);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(201, 16);
            this.label6.Text = "Código del nuevo contenedor";
            // 
            // LBL_MENSAJE
            // 
            this.LBL_MENSAJE.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.LBL_MENSAJE.Location = new System.Drawing.Point(3, 170);
            this.LBL_MENSAJE.Name = "LBL_MENSAJE";
            this.LBL_MENSAJE.Size = new System.Drawing.Size(161, 16);
            this.LBL_MENSAJE.Text = "Contenedor principal vacio";
            // 
            // DG_CONTENEDOR
            // 
            this.DG_CONTENEDOR.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.DG_CONTENEDOR.Location = new System.Drawing.Point(3, 189);
            this.DG_CONTENEDOR.Name = "DG_CONTENEDOR";
            this.DG_CONTENEDOR.RowHeadersVisible = false;
            this.DG_CONTENEDOR.Size = new System.Drawing.Size(231, 58);
            this.DG_CONTENEDOR.TabIndex = 28;
            this.DG_CONTENEDOR.TableStyles.Add(this.dataGridTableStyle1);
            // 
            // dataGridTableStyle1
            // 
            this.dataGridTableStyle1.MappingName = "HOLA";
            // 
            // btnDividir
            // 
            this.btnDividir.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.btnDividir.Location = new System.Drawing.Point(119, 248);
            this.btnDividir.Name = "btnDividir";
            this.btnDividir.Size = new System.Drawing.Size(70, 20);
            this.btnDividir.TabIndex = 43;
            this.btnDividir.Text = "Dividir";
            this.btnDividir.Click += new System.EventHandler(this.btnDividir_Click);
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.btnLimpiar.Location = new System.Drawing.Point(49, 248);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(70, 20);
            this.btnLimpiar.TabIndex = 45;
            this.btnLimpiar.Text = "Limpiar";
            this.btnLimpiar.Click += new System.EventHandler(this.btnLimpiar_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.button1.Location = new System.Drawing.Point(164, 168);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(70, 20);
            this.button1.TabIndex = 54;
            this.button1.Text = "Borrar";
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // FrmDvdirCntndoEmpque
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(238, 270);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtEstdoOrden);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnDividir);
            this.Controls.Add(this.btnLimpiar);
            this.Controls.Add(this.DG_CONTENEDOR);
            this.Controls.Add(this.txtCntndorNuevo);
            this.Controls.Add(this.txtCntdad);
            this.Controls.Add(this.txtCdgoPrdcto);
            this.Controls.Add(this.txtOrdenPrdccion);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtCntndorAntiguo);
            this.Controls.Add(this.pnlContenedor);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.LBL_MENSAJE);
            this.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.Name = "FrmDvdirCntndoEmpque";
            this.Text = "Producto Terminado";
            this.pnlContenedor.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlContenedor;
        private System.Windows.Forms.Label lblTitulo;
        private System.Windows.Forms.Panel pnlBordeRojo;
        private System.Windows.Forms.TextBox txtCntndorAntiguo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtOrdenPrdccion;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtCdgoPrdcto;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtEstdoOrden;
        private System.Windows.Forms.TextBox txtCntdad;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtCntndorNuevo;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label LBL_MENSAJE;
        private System.Windows.Forms.DataGrid DG_CONTENEDOR;
        private System.Windows.Forms.DataGridTableStyle dataGridTableStyle1;
        private System.Windows.Forms.Button btnDividir;
        private System.Windows.Forms.Button btnLimpiar;
        private System.Windows.Forms.Button button1;
    }
}