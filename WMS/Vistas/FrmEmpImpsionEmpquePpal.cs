﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using MobilePrinter;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Collections;
using System.Runtime;
using Microsoft.Win32;
using MobilePrinter.TSCWinCE;
using WMS.DTE;
using WMS.DTO;

namespace WMS.Vistas
{
    public partial class FrmEmpImpsionEmpquePpal : Form
    {
        public FrmEmpImpsionEmpquePpal()
        {
            InitializeComponent();
            mtdCambiarRadio();
        }

        public void mtdCambiarRadio()
        {
            if (rdbOrdenProdccion.Checked)
            {
                txtNmroOrden.Visible = true;
                cboClientes.Visible = false;
                txtNmroOrden.Focus();
                txtNmroOrden.Text = "";
                txtCodigo.Text = "";
                txtNombre.Text = "";
            }

            if (rdbCliente.Checked)
            {
                txtNmroOrden.Visible = false;
                txtNmroOrden.Text = "";
                txtCodigo.Text = "";
                txtNombre.Text = "";                
                mtdListarClientes();
                cboClientes.Visible = true;
                cboClientes.Focus();
            }
        }

        public void mtdListarClientes()
        {
            ClienteDTO dtoCliente = new ClienteDTO();
            ClienteDTE dteCliente = new ClienteDTE();

            try
            {

                //DataTable dt = dteCliente.listarClientes();

                //DataTable dtTable = new DataTable();
                //dtTable.Columns.Add("CODIGO", typeof(string));
                //dtTable.Columns.Add("DESCRIPCION", typeof(string));
                //lblCargando.Visible = true;
                
                //foreach (DataRow row in dt.Rows)
                //{
                //    dtTable.Rows.Add(row[0], row[1]);
                //    Thread.Sleep(0);
                //}

                //lblCargando.Visible = false;
                //cboClientes.DisplayMember = "DESCRIPCION";
                //cboClientes.ValueMember = "CODIGO";
                //cboClientes.DataSource = dtTable;
                //cboClientes.Update();

                try
                {

                    DataTable dt = dteCliente.listarClientes();
                    cboClientes.DataSource = dt;
                    cboClientes.DisplayMember = "rzon_scial";
                    cboClientes.ValueMember = "cdgo_clnte";

                }
                catch (Exception)
                {
                    txtCodigo.Text = "";
                    txtNombre.Text = ""; 
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e + "", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }
        }

        private void Print(String strTipoContenedor, String strCodigoDeBarras, DateTime dtmFecha)
        {
            TSCEthernet IP = new TSCEthernet();
            IP.openport("192.21.21.35", 9100);
            IP.clearbuffer();
            IP.sendcommand("<xpml><page quantity='0' pitch='38.9 mm'></xpml>SIZE 78.7 mm, 38.9 mm\n");
            IP.sendcommand("DIRECTION 0,0\n");
            IP.sendcommand("REFERENCE 0,0\n");
            IP.sendcommand("OFFSET 0 mm\n");
            IP.sendcommand("SET PEEL OFF\n");
            IP.sendcommand("SET CUTTER OFF\n");
            IP.sendcommand("<xpml></page></xpml><xpml><page quantity='1' pitch='38.9 mm'></xpml>SET TEAR ON\n");
            IP.sendcommand("CLS\n");
            IP.sendcommand("CODEPAGE 1252\n");
            IP.sendcommand("TEXT 767,396,\"0\",180,8,8,\"" + strTipoContenedor + "\"\n");
            IP.sendcommand("TEXT 722,396,\"0\",180,8,8,\"" + strCodigoDeBarras.Substring(0, 6) + "\"\n");
            IP.sendcommand("TEXT 231,388,\"3\",180,1,1,\"Finotex\"\n");
            DateTime utcdt = dtmFecha.ToUniversalTime();
            String format = "MM/dd/yyyy";
            IP.sendcommand("TEXT 276,355,\"3\",180,1,1,\"" + utcdt.ToString(format) + "\"\n");
            IP.sendcommand("PDF417 673,307,673,307,180,W4,H12,C3,T0,\"" + strCodigoDeBarras + "\"\n");            
            IP.sendcommand("PRINT 1,1\n");
            IP.sendcommand("<xpml></page></xpml><xpml><end/></xpml>\n");
            IP.closeport();
        }

        public void mdtRecuperarCliente(String ls_Origen, String ls_Parametro)
        {
            OrdenDTE dteOrden = new OrdenDTE();
            ClienteDTO clienteDTO = new ClienteDTO();
            try
            {
                clienteDTO = dteOrden.RecuperarCliente(ls_Origen, ls_Parametro);//Consultamos al cliente con el numero de la orden, este nos devuelve la clase cliente.
                txtCodigo.Text = clienteDTO._strCodigo;
                txtNombre.Text = clienteDTO._strRzonScial;
            }
            catch (Exception e)
            {
                txtCodigo.Text = "";
                txtNombre.Text = ""; 
            }
        }

        public void mtdGenerarCodigo()
        {
            ContenedorDTE dteContenedor = new ContenedorDTE();
            ContenedorDTO dtoContenedor = new ContenedorDTO();

            dtoContenedor = dteContenedor.GenerarContenedorPrincipal();

            Print(dtoContenedor._strTipo, dtoContenedor._strCdgoBarrasPDF417, dtoContenedor._dtmFecha);
        }

        private void rdbOrdenProdccion_CheckedChanged(object sender, EventArgs e)
        {
            mtdCambiarRadio();
        }

        private void rdbCliente_CheckedChanged(object sender, EventArgs e)
        {
            mtdCambiarRadio();
        }
        private void button1_Click_1(object sender, EventArgs e)
        {
            mtdGenerarCodigo();
        }

        private void TXT_NMRO_ORDEN_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (rdbOrdenProdccion.Checked)
                {
                    mdtRecuperarCliente("1",txtNmroOrden.Text);
                }
                if (rdbCliente.Checked)
                {
                    mdtRecuperarCliente("2",cboClientes.SelectedValue.ToString());
                }
            }
        }

        private void cboClientes_SelectedValueChanged(object sender, EventArgs e)
        {
            
        }
        private void cboClientes_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdbOrdenProdccion.Checked)
            {
                mdtRecuperarCliente("1", txtNmroOrden.Text);
            }
            if (rdbCliente.Checked)
            {
                try
                {
                    mdtRecuperarCliente("2", cboClientes.SelectedValue.ToString());
                }
                catch (Exception)
                {
                    txtCodigo.Text = "";
                    txtNombre.Text = "";   
                }
            }
        }
    }
}