﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Microsoft.Win32;
using MobilePrinter;
using MobilePrinter.TSCWinCE;
using WMS.DTE;
using WMS.DTO;

namespace WMS.Vistas
{
    public partial class FrmDvdirCntndor : Form
    {
        // Create new DataTable and DataSource objects.
        DataTable dt;

        private String ls_CntdadNva;

        public FrmDvdirCntndor()
        {
            InitializeComponent();

            ls_CntdadNva = "";

            DataGridTableStyle ts = new DataGridTableStyle();
            ts.MappingName = "Order";

            // Order date column style
            DataGridColumnStyle orderDate = new DataGridTextBoxColumn();
            orderDate.MappingName = "OrderDate";
            orderDate.HeaderText = "Date";
            orderDate.Width = (DG_CONTENEDOR.Width / 2) - 3;
            ts.GridColumnStyles.Add(orderDate);

            // Shipping name column style
            DataGridColumnStyle shipName = new DataGridTextBoxColumn();
            shipName.MappingName = "ShipName";
            shipName.HeaderText = "Customer";
            shipName.Width = (DG_CONTENEDOR.Width / 2) - 3;
            ts.GridColumnStyles.Add(shipName);

            DG_CONTENEDOR.TableStyles.Add(ts);

            dt = new DataTable("Order");
            dt.Columns.Add("OrderDate").ReadOnly = false;
            dt.Columns.Add("ShipName").ReadOnly = false;
            DG_CONTENEDOR.DataSource = dt;            
        }

        public void ConsultarOrdenDeProduccionPorCodigoContenedor()
        {
            ArticuloDTO articuloDTO = new ArticuloDTO();
            ContenedorDTO contenedorDTO = new ContenedorDTO();
            OrdenDTO ordenDTO = new OrdenDTO();

            EmpaqueDTE empaqueDTE = new EmpaqueDTE();
            OrdenDTE ordenDTE = new OrdenDTE();

            String lsCdigo = txtCntndor.Text;

            if (!(lsCdigo.Substring(0, 1).Equals("S") ||
                lsCdigo.Substring(0, 1).Equals("R") ||
                lsCdigo.Substring(0, 1).Equals("C") ||
                lsCdigo.Substring(0, 1).Equals("O")))
            {
                MessageBox.Show("La unidad de contenedor es invalida.", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                txtCntndor.Focus();//Paramos el codigo nuevamente en el campo que lee el codigo de barras.
                txtCntndor.SelectAll();
                return;
            }

            if (lsCdigo.Length < 32)
            {
                MessageBox.Show("La unidad de contenedor es invalida.", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                txtCntndor.Focus();//Paramos el codigo nuevamente en el campo que lee el codigo de barras.
                txtCntndor.SelectAll();
                return;
            }

            contenedorDTO._strCdgoBarrasPDF417 = txtCntndor.Text.Trim();
            articuloDTO = ordenDTE.ObtenerInformacionDeLaOrdenPorUnitContainer(contenedorDTO);

            if (articuloDTO._strFuente == null)
            {
                MessageBox.Show("No existe información para la unidad de contenedor.", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
            }
            else
            {
                if (articuloDTO._strFuente.Equals("1"))
                {
                    txtOrdenPrdccion.Text = articuloDTO._strClientDBProductionOrderId.ToString();
                }
                if (articuloDTO._strFuente.Equals("2"))
                {
                    txtOrdenPrdccion.Text = articuloDTO._strProductionOrderId.ToString();
                }
                txtCdgoPrdcto.Text = articuloDTO._lngCdgoArtclo.ToString();
                txtEstdoOrden.Text = articuloDTO._strEstado.ToString();
                txtCntdad.Text = articuloDTO._dblCntdadProducida.ToString();
            }
        }

        public void Agregar()
        {
            dt.Rows.Add(txtCantContenedor.Text, txtCantUnidades.Text);
            SumarUnidadCantidadTotal();
            txtCantContenedor.Text = "";
            txtCantUnidades.Text = "";
        }

        public void SumarUnidadCantidadTotal()
        {
            Double ldbl_TtalUndadesCntnidas = 0;
            foreach (DataRow row in dt.Rows)
            {
                ldbl_TtalUndadesCntnidas = ldbl_TtalUndadesCntnidas + Double.Parse(row[1].ToString());
            }
            txtCantDivdir.Text = ldbl_TtalUndadesCntnidas.ToString();
        }

        public void Guardar()
        {
            EmpaqueDTE empaqueDTE = new EmpaqueDTE();
            IdentificadorDTO contendorDTO = new IdentificadorDTO();

            Double ldbl_cantContndor = Double.Parse(txtCntdad.Text);
            Double ldbl_cantDivdir = Double.Parse(txtCantDivdir.Text);

            String lsCdigo = txtCntndor.Text;
            Double ldbl_undadDvdir = 0;
            Double ldbl_CntdadDvdir = 0;

            if (ldbl_cantContndor<=0)
            {
                MessageBox.Show("La cantida del contenedor debe ser mayo a cero.", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                return;
            }

            if (ldbl_cantDivdir <= 0)
            {
                MessageBox.Show("La cantida a dividir debe ser mayo a cero.", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                return;
            }

            if (ldbl_cantContndor != ldbl_cantDivdir)
            {
                MessageBox.Show("Las unidades contenidas deben ser igual a la cantidad a dividir.", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                return;
            }

            String ls_Cdigo = txtCntndor.Text;
            if (ls_Cdigo.Length < 32 || ls_Cdigo.Length > 32)
            {
                MessageBox.Show("El código de la unidad contenedora es invalida.", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                return;
            }

            if (!lsCdigo.Substring(0, 1).Equals("S") && !lsCdigo.Substring(0, 1).Equals("R") && !lsCdigo.Substring(0, 1).Equals("C") && !lsCdigo.Substring(0, 1).Equals("O"))
            {
                MessageBox.Show("Solo pueden ser escaneadas unidades de contenedor. Las unidades de contenedores debe empesar con S,R,C,O.", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                return;
            }

            
            Int32 cont = 0;
            foreach (DataRow row in dt.Rows)
            {
                ldbl_undadDvdir = Double.Parse(row[0].ToString());
                ldbl_CntdadDvdir = Double.Parse(row[1].ToString());

                if (ldbl_undadDvdir <= 0 && ldbl_CntdadDvdir <= 0)
                {
                    dt.Rows.RemoveAt(cont);
                }
                if (ldbl_undadDvdir <= 0)
                {
                    MessageBox.Show("El número de contenedores debe ser mayor a cero.", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                    return;
                }
                if (ldbl_CntdadDvdir <= 0)
                {
                    MessageBox.Show("Las cantidades contenidas debe ser mayor a cero.", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                    return;
                }
            }

            //Inicio: Armo la cadena que voy a enviar al procedimiento:

            String ls_LstaCdgoCntndores = ls_Cdigo;
            string fmt = "000000.00";
            foreach (DataRow row in dt.Rows)
            {
                ldbl_undadDvdir = Double.Parse(row[0].ToString());
                ldbl_CntdadDvdir = Double.Parse(row[1].ToString());

                for (int x = 1; x <= ldbl_undadDvdir; x++)
                {
                    //ls_CntdadNva = Convert.ToString(ldbl_CntdadDvdir.ToString("D")) + (9 - Convert.ToString(ldbl_CntdadDvdir).Length);
                    ls_CntdadNva = ldbl_CntdadDvdir.ToString(fmt);
                    ls_LstaCdgoCntndores = ls_LstaCdgoCntndores + ls_CntdadNva;
                }
            }

            //Fin: Armo la cadena que voy a enviar al procedimiento:

            DataTable dt_StikerGenerados = empaqueDTE.DividirContenedor(ls_LstaCdgoCntndores);
            foreach (DataRow row in dt_StikerGenerados.Rows)
            {
                Print(row[1].ToString(), row[2].ToString(), row[3].ToString(), row[29].ToString(),
                        row[4].ToString(), row[0].ToString(), row[46].ToString(), row[45].ToString(),
                        row[44].ToString(), row[43].ToString(), row[49].ToString(), row[39].ToString(),
                        row[10].ToString(), row[11].ToString(), row[7].ToString(), row[42].ToString());//Lamamos el metodo que imprime los stikers
            }
        }

        private void Print(String strUnitContainerType, String strProductCode, String strUnits,
                            String strUnidMedida, String strCustomerProductCode, String strInventoryItemBarCode,
                            String strSubLneaEsHeatTranfer, String strPrecion, String strTiempo, String strTemperatura,
                            String strQuitarCdgoCliente, String strCdgpDisnoClnte, String strDocumentSource,
                            String strClientDBProductionOrderId, String strProductionOrdenId, String strOrdenCompra)
        {
            TSCEthernet IP = new TSCEthernet();
            IP.openport("192.21.21.35", 9100);
            IP.sendcommand("<xpml><page quantity='0' pitch='38.9 mm'></xpml>SIZE 78.7 mm, 38.9 mm\n");
            IP.sendcommand("DIRECTION 0,0\n");
            IP.sendcommand("REFERENCE 0,0\n");
            IP.sendcommand("OFFSET 0 mm\n");
            IP.sendcommand("SET PEEL OFF\n");
            IP.sendcommand("SET CUTTER OFF\n");
            IP.sendcommand("<xpml></page></xpml><xpml><page quantity='1' pitch='38.9 mm'></xpml>SET TEAR ON\n");
            IP.sendcommand("CLS\n");
            IP.sendcommand("CODEPAGE 1252\n");
            IP.sendcommand("TEXT 767,396,\"0\",180,8,8,\"" + strUnitContainerType + "\"\n");
            IP.sendcommand("TEXT 730,398,\"0\",180,8,8,\"" + strProductCode + "\"\n");
            IP.sendcommand("TEXT 428,396,\"0\",180,8,8,\"" + strUnits + " " + strUnidMedida + "\"\n");
            IP.sendcommand("TEXT 230,399,\"0\",180,8,8,\"" + strCustomerProductCode + "\"\n");

            //Inicio si la linea es heattransfer pintamos las tres lineas de codigo
            if (strSubLneaEsHeatTranfer.Equals("S"))
            {
                IP.sendcommand("TEXT 766,349,\"3\",180,1,1,\"" + strPrecion + "\"\n");
                IP.sendcommand("TEXT 767,316,\"3\",180,1,1,\"" + strTemperatura + "\"\n");
                IP.sendcommand("TEXT 770,286,\"3\",180,1,1,\"" + strTiempo + "\"\n");
            }
            //Fin si la linea es heattransfer pintamos las tres lineas de codigo

            if (!strQuitarCdgoCliente.Equals("S"))
            {
                IP.sendcommand("TEXT 759,199,\"0\",180,8,8,\"Lot\"\n");
                IP.sendcommand("TEXT 757,170,\"0\",180,8,8,\"Cust.Ref\"\n");
                IP.sendcommand("TEXT 758,134,\"0\",180,8,8,\"PO\"\n");
                if (strDocumentSource.Equals(1))
                {
                    IP.sendcommand("TEXT 708,196,\"3\",180,1,1,\"" + strClientDBProductionOrderId + "\"\n");
                }
                else
                {
                    IP.sendcommand("TEXT 708,196,\"3\",180,1,1,\"" + strProductionOrdenId + "\"\n");
                }

                IP.sendcommand("TEXT 632,166,\"3\",180,1,1,\"" + strCdgpDisnoClnte + "\"\n");
                IP.sendcommand("TEXT 709,133,\"3\",180,1,1,\"" + strOrdenCompra + "\"\n");
            }
            IP.sendcommand("TEXT 178,169,\"3\",180,1,1,\"Finotex\"\n");

            DateTime dt = DateTime.Now;
            DateTime utcdt = dt.ToUniversalTime();
            String format = "MM/dd/yyyy";

            IP.sendcommand("TEXT 225,133,\"3\",180,1,1,\"" + dt.ToString(format) + "\"\n");

            IP.sendcommand("PDF417 542,341,542,341,180,W4,H12,C3,T0,\"" + strInventoryItemBarCode + "\"\n");//Imprimirmo el codigo de barras

            IP.sendcommand("PRINT 1,1\n");
            IP.sendcommand("<xpml></page></xpml><xpml><end/></xpml>\n");

            IP.closeport();
        }

        private void txtCntndorAntiguo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                ConsultarOrdenDeProduccionPorCodigoContenedor();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Agregar();            
        }

        private void btnDividir_Click(object sender, EventArgs e)
        {
            Guardar();
        }
    }
}