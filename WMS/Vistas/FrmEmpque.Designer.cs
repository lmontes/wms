﻿namespace WMS.Vistas
{
    partial class FrmEmpque
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DG_CONTENEDOR = new System.Windows.Forms.DataGrid();
            this.dataGridTableStyle1 = new System.Windows.Forms.DataGridTableStyle();
            this.dataGridTextBoxColumn1 = new System.Windows.Forms.DataGridTextBoxColumn();
            this.txtCdgobrras = new System.Windows.Forms.TextBox();
            this.txtCodLoclzcion = new System.Windows.Forms.TextBox();
            this.txtContenedor = new System.Windows.Forms.TextBox();
            this.LBL_MENSAJE = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pnlContenedor = new System.Windows.Forms.Panel();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.pnlBordeRojo = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.txtPeso = new System.Windows.Forms.TextBox();
            this.btnDesempacar = new System.Windows.Forms.Button();
            this.btnEmpacar = new System.Windows.Forms.Button();
            this.btnLimpiarEmpacar = new System.Windows.Forms.Button();
            this.btnMover = new System.Windows.Forms.Button();
            this.btnLimpiarMover = new System.Windows.Forms.Button();
            this.pnlContenedor.SuspendLayout();
            this.SuspendLayout();
            // 
            // DG_CONTENEDOR
            // 
            this.DG_CONTENEDOR.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.DG_CONTENEDOR.Location = new System.Drawing.Point(3, 111);
            this.DG_CONTENEDOR.Name = "DG_CONTENEDOR";
            this.DG_CONTENEDOR.RowHeadersVisible = false;
            this.DG_CONTENEDOR.Size = new System.Drawing.Size(232, 109);
            this.DG_CONTENEDOR.TabIndex = 0;
            this.DG_CONTENEDOR.TableStyles.Add(this.dataGridTableStyle1);
            // 
            // dataGridTableStyle1
            // 
            this.dataGridTableStyle1.GridColumnStyles.Add(this.dataGridTextBoxColumn1);
            this.dataGridTableStyle1.MappingName = "HOLA";
            // 
            // dataGridTextBoxColumn1
            // 
            this.dataGridTextBoxColumn1.Format = "";
            this.dataGridTextBoxColumn1.FormatInfo = null;
            this.dataGridTextBoxColumn1.HeaderText = "JHGJHGJHGJHGJH";
            this.dataGridTextBoxColumn1.NullText = "HJHGJHGJHGJHG";
            this.dataGridTextBoxColumn1.Width = 100;
            // 
            // txtCdgobrras
            // 
            this.txtCdgobrras.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtCdgobrras.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.txtCdgobrras.ForeColor = System.Drawing.Color.Black;
            this.txtCdgobrras.Location = new System.Drawing.Point(3, 32);
            this.txtCdgobrras.Name = "txtCdgobrras";
            this.txtCdgobrras.Size = new System.Drawing.Size(232, 19);
            this.txtCdgobrras.TabIndex = 5;
            this.txtCdgobrras.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TXT_CODIGO_KeyPress);
            // 
            // txtCodLoclzcion
            // 
            this.txtCodLoclzcion.BackColor = System.Drawing.Color.Silver;
            this.txtCodLoclzcion.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.txtCodLoclzcion.Location = new System.Drawing.Point(76, 53);
            this.txtCodLoclzcion.Name = "txtCodLoclzcion";
            this.txtCodLoclzcion.ReadOnly = true;
            this.txtCodLoclzcion.Size = new System.Drawing.Size(159, 19);
            this.txtCodLoclzcion.TabIndex = 6;
            // 
            // txtContenedor
            // 
            this.txtContenedor.BackColor = System.Drawing.Color.Silver;
            this.txtContenedor.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.txtContenedor.Location = new System.Drawing.Point(76, 72);
            this.txtContenedor.Name = "txtContenedor";
            this.txtContenedor.ReadOnly = true;
            this.txtContenedor.Size = new System.Drawing.Size(159, 19);
            this.txtContenedor.TabIndex = 7;
            // 
            // LBL_MENSAJE
            // 
            this.LBL_MENSAJE.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.LBL_MENSAJE.Location = new System.Drawing.Point(3, 94);
            this.LBL_MENSAJE.Name = "LBL_MENSAJE";
            this.LBL_MENSAJE.Size = new System.Drawing.Size(161, 16);
            this.LBL_MENSAJE.Text = "Contenedor principal vacio";
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label1.Location = new System.Drawing.Point(3, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 16);
            this.label1.Text = "Localizacion";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label2.Location = new System.Drawing.Point(3, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 16);
            this.label2.Text = "Contenedor";
            // 
            // pnlContenedor
            // 
            this.pnlContenedor.BackColor = System.Drawing.Color.DimGray;
            this.pnlContenedor.Controls.Add(this.lblTitulo);
            this.pnlContenedor.Controls.Add(this.pnlBordeRojo);
            this.pnlContenedor.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlContenedor.Location = new System.Drawing.Point(0, 0);
            this.pnlContenedor.Name = "pnlContenedor";
            this.pnlContenedor.Size = new System.Drawing.Size(238, 26);
            // 
            // lblTitulo
            // 
            this.lblTitulo.ForeColor = System.Drawing.Color.White;
            this.lblTitulo.Location = new System.Drawing.Point(3, 3);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(208, 20);
            this.lblTitulo.Text = "Proceso de empaque";
            // 
            // pnlBordeRojo
            // 
            this.pnlBordeRojo.BackColor = System.Drawing.Color.Brown;
            this.pnlBordeRojo.Location = new System.Drawing.Point(0, 24);
            this.pnlBordeRojo.Name = "pnlBordeRojo";
            this.pnlBordeRojo.Size = new System.Drawing.Size(238, 3);
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label4.Location = new System.Drawing.Point(0, 223);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(161, 16);
            this.label4.Text = "Peso contenedor principal";
            // 
            // txtPeso
            // 
            this.txtPeso.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.txtPeso.Location = new System.Drawing.Point(131, 221);
            this.txtPeso.Name = "txtPeso";
            this.txtPeso.ReadOnly = true;
            this.txtPeso.Size = new System.Drawing.Size(63, 19);
            this.txtPeso.TabIndex = 13;
            // 
            // btnDesempacar
            // 
            this.btnDesempacar.Enabled = false;
            this.btnDesempacar.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.btnDesempacar.Location = new System.Drawing.Point(84, 241);
            this.btnDesempacar.Name = "btnDesempacar";
            this.btnDesempacar.Size = new System.Drawing.Size(70, 20);
            this.btnDesempacar.TabIndex = 35;
            this.btnDesempacar.Text = "Desempacar";
            this.btnDesempacar.Visible = false;
            this.btnDesempacar.Click += new System.EventHandler(this.btnGenerarStiker_Click);
            // 
            // btnEmpacar
            // 
            this.btnEmpacar.Enabled = false;
            this.btnEmpacar.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.btnEmpacar.Location = new System.Drawing.Point(154, 241);
            this.btnEmpacar.Name = "btnEmpacar";
            this.btnEmpacar.Size = new System.Drawing.Size(70, 20);
            this.btnEmpacar.TabIndex = 36;
            this.btnEmpacar.Text = "Empacar";
            this.btnEmpacar.Visible = false;
            this.btnEmpacar.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // btnLimpiarEmpacar
            // 
            this.btnLimpiarEmpacar.Enabled = false;
            this.btnLimpiarEmpacar.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.btnLimpiarEmpacar.Location = new System.Drawing.Point(14, 241);
            this.btnLimpiarEmpacar.Name = "btnLimpiarEmpacar";
            this.btnLimpiarEmpacar.Size = new System.Drawing.Size(70, 20);
            this.btnLimpiarEmpacar.TabIndex = 42;
            this.btnLimpiarEmpacar.Text = "Limpiar";
            this.btnLimpiarEmpacar.Visible = false;
            // 
            // btnMover
            // 
            this.btnMover.Enabled = false;
            this.btnMover.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.btnMover.Location = new System.Drawing.Point(119, 242);
            this.btnMover.Name = "btnMover";
            this.btnMover.Size = new System.Drawing.Size(70, 20);
            this.btnMover.TabIndex = 43;
            this.btnMover.Text = "Mover";
            this.btnMover.Visible = false;
            this.btnMover.Click += new System.EventHandler(this.btnMover_Click);
            // 
            // btnLimpiarMover
            // 
            this.btnLimpiarMover.Enabled = false;
            this.btnLimpiarMover.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.btnLimpiarMover.Location = new System.Drawing.Point(49, 242);
            this.btnLimpiarMover.Name = "btnLimpiarMover";
            this.btnLimpiarMover.Size = new System.Drawing.Size(70, 20);
            this.btnLimpiarMover.TabIndex = 44;
            this.btnLimpiarMover.Text = "Limpiar";
            this.btnLimpiarMover.Visible = false;
            // 
            // FrmEmpque
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(238, 270);
            this.Controls.Add(this.txtPeso);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.pnlContenedor);
            this.Controls.Add(this.LBL_MENSAJE);
            this.Controls.Add(this.txtContenedor);
            this.Controls.Add(this.txtCodLoclzcion);
            this.Controls.Add(this.txtCdgobrras);
            this.Controls.Add(this.DG_CONTENEDOR);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnEmpacar);
            this.Controls.Add(this.btnDesempacar);
            this.Controls.Add(this.btnLimpiarEmpacar);
            this.Controls.Add(this.btnLimpiarMover);
            this.Controls.Add(this.btnMover);
            this.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.Name = "FrmEmpque";
            this.Text = "Producto termiando";
            this.pnlContenedor.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGrid DG_CONTENEDOR;
        private System.Windows.Forms.DataGridTableStyle dataGridTableStyle1;
        private System.Windows.Forms.DataGridTextBoxColumn dataGridTextBoxColumn1;
        private System.Windows.Forms.TextBox txtCdgobrras;
        private System.Windows.Forms.TextBox txtCodLoclzcion;
        private System.Windows.Forms.TextBox txtContenedor;
        private System.Windows.Forms.Label LBL_MENSAJE;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel pnlContenedor;
        private System.Windows.Forms.Label lblTitulo;
        private System.Windows.Forms.Panel pnlBordeRojo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtPeso;
        private System.Windows.Forms.Button btnDesempacar;
        private System.Windows.Forms.Button btnEmpacar;
        private System.Windows.Forms.Button btnLimpiarEmpacar;
        private System.Windows.Forms.Button btnMover;
        private System.Windows.Forms.Button btnLimpiarMover;
    }
}