﻿namespace WMS.Vistas
{
    partial class FrmPreCut
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.cbo_Origen = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.TXT_ORDER_ID = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnGenerarStiker = new System.Windows.Forms.Button();
            this.TXT_COLOR = new System.Windows.Forms.TextBox();
            this.pnlContenedor = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.pnlBordeRojo = new System.Windows.Forms.Panel();
            this.TXT_ALTO = new System.Windows.Forms.TextBox();
            this.NO_CNTNDOR = new System.Windows.Forms.TextBox();
            this.TXT_NMBRE_CLDAD = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.TXT_SUB_LNEA_INVNTRIO = new System.Windows.Forms.TextBox();
            this.TXT_ANCHO = new System.Windows.Forms.TextBox();
            this.TXT_CNTIDAD_POR_CNTENDOR = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.TXT_NMBRE_DSNO = new System.Windows.Forms.TextBox();
            this.cbo_tpo_cntndor = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.TXT_COD_PRODUCTO = new System.Windows.Forms.TextBox();
            this.TXT_CNTIDAD_PRDCDA = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtOrdenWms = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.pnlContenedor.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label1.Location = new System.Drawing.Point(9, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 18);
            this.label1.Text = "Source";
            // 
            // cbo_Origen
            // 
            this.cbo_Origen.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.cbo_Origen.Location = new System.Drawing.Point(67, 31);
            this.cbo_Origen.Name = "cbo_Origen";
            this.cbo_Origen.Size = new System.Drawing.Size(164, 19);
            this.cbo_Origen.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label2.Location = new System.Drawing.Point(9, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 18);
            this.label2.Text = "Order ID";
            // 
            // TXT_ORDER_ID
            // 
            this.TXT_ORDER_ID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.TXT_ORDER_ID.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.TXT_ORDER_ID.Location = new System.Drawing.Point(67, 53);
            this.TXT_ORDER_ID.Name = "TXT_ORDER_ID";
            this.TXT_ORDER_ID.Size = new System.Drawing.Size(164, 19);
            this.TXT_ORDER_ID.TabIndex = 4;
            this.TXT_ORDER_ID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TXT_ORDER_ID_KeyPress);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnGenerarStiker);
            this.panel1.Controls.Add(this.TXT_COLOR);
            this.panel1.Controls.Add(this.pnlContenedor);
            this.panel1.Controls.Add(this.TXT_ALTO);
            this.panel1.Controls.Add(this.NO_CNTNDOR);
            this.panel1.Controls.Add(this.TXT_NMBRE_CLDAD);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.TXT_SUB_LNEA_INVNTRIO);
            this.panel1.Controls.Add(this.TXT_ANCHO);
            this.panel1.Controls.Add(this.TXT_CNTIDAD_POR_CNTENDOR);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.TXT_NMBRE_DSNO);
            this.panel1.Controls.Add(this.cbo_tpo_cntndor);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.TXT_COD_PRODUCTO);
            this.panel1.Controls.Add(this.TXT_CNTIDAD_PRDCDA);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.cbo_Origen);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.TXT_ORDER_ID);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.txtOrdenWms);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(638, 455);
            // 
            // btnGenerarStiker
            // 
            this.btnGenerarStiker.Enabled = false;
            this.btnGenerarStiker.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.btnGenerarStiker.Location = new System.Drawing.Point(9, 247);
            this.btnGenerarStiker.Name = "btnGenerarStiker";
            this.btnGenerarStiker.Size = new System.Drawing.Size(222, 20);
            this.btnGenerarStiker.TabIndex = 34;
            this.btnGenerarStiker.Text = "Generar stiker";
            this.btnGenerarStiker.Click += new System.EventHandler(this.button1_Click);
            // 
            // TXT_COLOR
            // 
            this.TXT_COLOR.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.TXT_COLOR.Location = new System.Drawing.Point(72, 125);
            this.TXT_COLOR.Name = "TXT_COLOR";
            this.TXT_COLOR.ReadOnly = true;
            this.TXT_COLOR.Size = new System.Drawing.Size(77, 19);
            this.TXT_COLOR.TabIndex = 23;
            // 
            // pnlContenedor
            // 
            this.pnlContenedor.BackColor = System.Drawing.Color.DimGray;
            this.pnlContenedor.Controls.Add(this.label14);
            this.pnlContenedor.Controls.Add(this.pnlBordeRojo);
            this.pnlContenedor.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlContenedor.Location = new System.Drawing.Point(0, 0);
            this.pnlContenedor.Name = "pnlContenedor";
            this.pnlContenedor.Size = new System.Drawing.Size(638, 26);
            // 
            // label14
            // 
            this.label14.ForeColor = System.Drawing.Color.White;
            this.label14.Location = new System.Drawing.Point(3, 4);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(150, 20);
            this.label14.Text = "Impresion de estiker";
            // 
            // pnlBordeRojo
            // 
            this.pnlBordeRojo.BackColor = System.Drawing.Color.Brown;
            this.pnlBordeRojo.Location = new System.Drawing.Point(0, 24);
            this.pnlBordeRojo.Name = "pnlBordeRojo";
            this.pnlBordeRojo.Size = new System.Drawing.Size(238, 3);
            // 
            // TXT_ALTO
            // 
            this.TXT_ALTO.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.TXT_ALTO.Location = new System.Drawing.Point(190, 144);
            this.TXT_ALTO.Name = "TXT_ALTO";
            this.TXT_ALTO.ReadOnly = true;
            this.TXT_ALTO.Size = new System.Drawing.Size(41, 19);
            this.TXT_ALTO.TabIndex = 20;
            // 
            // NO_CNTNDOR
            // 
            this.NO_CNTNDOR.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.NO_CNTNDOR.Location = new System.Drawing.Point(148, 222);
            this.NO_CNTNDOR.Name = "NO_CNTNDOR";
            this.NO_CNTNDOR.ReadOnly = true;
            this.NO_CNTNDOR.Size = new System.Drawing.Size(83, 19);
            this.NO_CNTNDOR.TabIndex = 37;
            // 
            // TXT_NMBRE_CLDAD
            // 
            this.TXT_NMBRE_CLDAD.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.TXT_NMBRE_CLDAD.Location = new System.Drawing.Point(190, 125);
            this.TXT_NMBRE_CLDAD.Name = "TXT_NMBRE_CLDAD";
            this.TXT_NMBRE_CLDAD.ReadOnly = true;
            this.TXT_NMBRE_CLDAD.Size = new System.Drawing.Size(41, 19);
            this.TXT_NMBRE_CLDAD.TabIndex = 12;
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label15.Location = new System.Drawing.Point(9, 223);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(128, 20);
            this.label15.Text = "Contenedores esperados";
            // 
            // TXT_SUB_LNEA_INVNTRIO
            // 
            this.TXT_SUB_LNEA_INVNTRIO.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.TXT_SUB_LNEA_INVNTRIO.Location = new System.Drawing.Point(72, 144);
            this.TXT_SUB_LNEA_INVNTRIO.Name = "TXT_SUB_LNEA_INVNTRIO";
            this.TXT_SUB_LNEA_INVNTRIO.ReadOnly = true;
            this.TXT_SUB_LNEA_INVNTRIO.Size = new System.Drawing.Size(63, 19);
            this.TXT_SUB_LNEA_INVNTRIO.TabIndex = 11;
            // 
            // TXT_ANCHO
            // 
            this.TXT_ANCHO.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.TXT_ANCHO.Location = new System.Drawing.Point(148, 144);
            this.TXT_ANCHO.Name = "TXT_ANCHO";
            this.TXT_ANCHO.ReadOnly = true;
            this.TXT_ANCHO.Size = new System.Drawing.Size(32, 19);
            this.TXT_ANCHO.TabIndex = 17;
            // 
            // TXT_CNTIDAD_POR_CNTENDOR
            // 
            this.TXT_CNTIDAD_POR_CNTENDOR.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.TXT_CNTIDAD_POR_CNTENDOR.Location = new System.Drawing.Point(148, 203);
            this.TXT_CNTIDAD_POR_CNTENDOR.Name = "TXT_CNTIDAD_POR_CNTENDOR";
            this.TXT_CNTIDAD_POR_CNTENDOR.Size = new System.Drawing.Size(83, 19);
            this.TXT_CNTIDAD_POR_CNTENDOR.TabIndex = 32;
            this.TXT_CNTIDAD_POR_CNTENDOR.Validated += new System.EventHandler(this.TXT_CNTIDAD_POR_CNTENDOR_Validated);
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label5.Location = new System.Drawing.Point(9, 143);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 20);
            this.label5.Text = "Linea";
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label13.Location = new System.Drawing.Point(9, 204);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(135, 18);
            this.label13.Text = "Unidades por contenedor";
            // 
            // TXT_NMBRE_DSNO
            // 
            this.TXT_NMBRE_DSNO.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.TXT_NMBRE_DSNO.Location = new System.Drawing.Point(72, 106);
            this.TXT_NMBRE_DSNO.Name = "TXT_NMBRE_DSNO";
            this.TXT_NMBRE_DSNO.ReadOnly = true;
            this.TXT_NMBRE_DSNO.Size = new System.Drawing.Size(159, 19);
            this.TXT_NMBRE_DSNO.TabIndex = 8;
            // 
            // cbo_tpo_cntndor
            // 
            this.cbo_tpo_cntndor.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.cbo_tpo_cntndor.Location = new System.Drawing.Point(148, 183);
            this.cbo_tpo_cntndor.Name = "cbo_tpo_cntndor";
            this.cbo_tpo_cntndor.Size = new System.Drawing.Size(83, 19);
            this.cbo_tpo_cntndor.TabIndex = 27;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label4.Location = new System.Drawing.Point(9, 107);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 20);
            this.label4.Text = "Descripcion";
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label12.Location = new System.Drawing.Point(9, 185);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(108, 18);
            this.label12.Text = "Tipo de contenedor";
            // 
            // TXT_COD_PRODUCTO
            // 
            this.TXT_COD_PRODUCTO.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.TXT_COD_PRODUCTO.Location = new System.Drawing.Point(72, 88);
            this.TXT_COD_PRODUCTO.Name = "TXT_COD_PRODUCTO";
            this.TXT_COD_PRODUCTO.ReadOnly = true;
            this.TXT_COD_PRODUCTO.Size = new System.Drawing.Size(159, 19);
            this.TXT_COD_PRODUCTO.TabIndex = 5;
            // 
            // TXT_CNTIDAD_PRDCDA
            // 
            this.TXT_CNTIDAD_PRDCDA.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.TXT_CNTIDAD_PRDCDA.Location = new System.Drawing.Point(148, 165);
            this.TXT_CNTIDAD_PRDCDA.Name = "TXT_CNTIDAD_PRDCDA";
            this.TXT_CNTIDAD_PRDCDA.ReadOnly = true;
            this.TXT_CNTIDAD_PRDCDA.Size = new System.Drawing.Size(83, 19);
            this.TXT_CNTIDAD_PRDCDA.TabIndex = 24;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label3.Location = new System.Drawing.Point(9, 89);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 20);
            this.label3.Text = "Producto";
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label9.Location = new System.Drawing.Point(9, 126);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(48, 20);
            this.label9.Text = "Color";
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label11.Location = new System.Drawing.Point(9, 167);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(108, 20);
            this.label11.Text = "Cantidad producida";
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label6.Location = new System.Drawing.Point(150, 127);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 20);
            this.label6.Text = "Calidad";
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label7.Location = new System.Drawing.Point(135, 147);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(15, 20);
            this.label7.Text = "W";
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label8.Location = new System.Drawing.Point(180, 146);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(11, 20);
            this.label8.Text = "H";
            // 
            // txtOrdenWms
            // 
            this.txtOrdenWms.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.txtOrdenWms.Location = new System.Drawing.Point(217, 248);
            this.txtOrdenWms.Name = "txtOrdenWms";
            this.txtOrdenWms.ReadOnly = true;
            this.txtOrdenWms.Size = new System.Drawing.Size(13, 19);
            this.txtOrdenWms.TabIndex = 46;
            this.txtOrdenWms.Visible = false;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Tahoma", 8F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic)
                            | System.Drawing.FontStyle.Underline))));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label10.Location = new System.Drawing.Point(9, 72);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(172, 13);
            this.label10.Text = "Información del producto:";
            // 
            // FrmPreCut
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(638, 455);
            this.Controls.Add(this.panel1);
            this.Name = "FrmPreCut";
            this.Text = "Finotex";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.panel1.ResumeLayout(false);
            this.pnlContenedor.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbo_Origen;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TXT_ORDER_ID;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox TXT_NMBRE_DSNO;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TXT_COD_PRODUCTO;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TXT_COLOR;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox TXT_ALTO;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox TXT_ANCHO;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox TXT_NMBRE_CLDAD;
        private System.Windows.Forms.TextBox TXT_SUB_LNEA_INVNTRIO;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cbo_tpo_cntndor;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox TXT_CNTIDAD_PRDCDA;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox TXT_CNTIDAD_POR_CNTENDOR;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button btnGenerarStiker;
        private System.Windows.Forms.TextBox NO_CNTNDOR;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Panel pnlContenedor;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Panel pnlBordeRojo;
        private System.Windows.Forms.TextBox txtOrdenWms;
    }
}