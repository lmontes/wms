﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Microsoft.Win32;
using MobilePrinter;
using MobilePrinter.TSCWinCE;
using WMS.DTE;
using WMS.DTO;

namespace WMS.Vistas
{
    public partial class FrmPreCut : Form
    {
        public FrmPreCut()
        {
            InitializeComponent();

            CargarOrigen();
            TiposDeContenedor();
        }

        public void CargarOrigen()
        {
            DataTable dtTable = new DataTable();
            dtTable.Columns.Add("CODIGO", typeof(int));
            dtTable.Columns.Add("DESCRIPCION", typeof(string));
            dtTable.Rows.Add(1, "Produccion");
            dtTable.Rows.Add(2, "Inventario");

            cbo_Origen.DisplayMember = "DESCRIPCION";
            cbo_Origen.ValueMember = "CODIGO";
            cbo_Origen.DataSource = dtTable;
        }

        public void TiposDeContenedor()
        {
            DataTable dtTable = new DataTable();
            dtTable.Columns.Add("CODIGO", typeof(string));
            dtTable.Columns.Add("DESCRIPCION", typeof(string));
            dtTable.Rows.Add("S", "Sleev");
            dtTable.Rows.Add("R", "Roll");
            dtTable.Rows.Add("C", "Cone");
            dtTable.Rows.Add("O", "Otro");

            cbo_tpo_cntndor.DisplayMember = "DESCRIPCION";
            cbo_tpo_cntndor.ValueMember = "CODIGO";
            cbo_tpo_cntndor.DataSource = dtTable;

        }

        public void ConsultarOrdenProduccion()
        {
            OrdenDTE ordenDTE = new OrdenDTE();
            
            OrdenDTO ordenDTO = new OrdenDTO();
            ArticuloDTO articuloDTO = new ArticuloDTO();

            try
            {
                ordenDTO._strClientDBProductionOrderId = TXT_ORDER_ID.Text;
                ordenDTO._strOrigen = cbo_Origen.SelectedValue.ToString();

                articuloDTO = ordenDTE.ConsultarOrden(ordenDTO);

                if (articuloDTO._lngCdgoArtclo != null)
                {
                    TXT_COD_PRODUCTO.Text = articuloDTO._lngCdgoArtclo.ToString();
                    TXT_NMBRE_DSNO.Text = articuloDTO._strNmbreArtclo.ToString();
                    TXT_ANCHO.Text = articuloDTO._strAncho.ToString();
                    TXT_ALTO.Text = articuloDTO._strLrgo.ToString();
                    TXT_COLOR.Text = articuloDTO._strClor.ToString();
                    TXT_SUB_LNEA_INVNTRIO.Text = articuloDTO._strNmbreSubLneaInv.ToString();
                    TXT_NMBRE_CLDAD.Text = articuloDTO._strNmbreCldad.ToString();
                    TXT_CNTIDAD_PRDCDA.Text = articuloDTO._dblCntdadProducida.ToString();
                    txtOrdenWms.Text = articuloDTO._strProductionOrderId.ToString();//Orden de WMS
                }
                else
                {
                    MessageBox.Show("La orden de produccion" + ordenDTO._strClientDBProductionOrderId + " no exite.", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                    TXT_ORDER_ID.Text = "";
                }
            }
            catch (Exception e) 
            {
                MessageBox.Show(e+"", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }
        }

        private void mtdGenerarEstiquer()//Metodo para generar los estiquer
        {
            ContenedorDTO contenedorDTO = new ContenedorDTO();
            OrdenDTO ordenDTO = new OrdenDTO();

            ContenedorDTE contenedorDTE = new ContenedorDTE();
            
            contenedorDTO._strTipo = cbo_tpo_cntndor.SelectedValue.ToString();//capturamos el tipo de contenedor, puede ser un rollo, sleev
            ordenDTO._strProductionOrderId = txtOrdenWms.Text.Trim().ToString();//Asignamos al DT la orden de WMS.
            contenedorDTO._dblCantContenidas = Double.Parse(TXT_CNTIDAD_POR_CNTENDOR.Text.Trim().ToString());//la cantidad total de contenedores a generar
            Double ld_Cant=Double.Parse(NO_CNTNDOR.Text);//la cantidad total de contenedores a generar
            
            contenedorDTO = contenedorDTE.generarEstiquer(contenedorDTO, ordenDTO, ld_Cant);//metodo que llamar el sp que generar stiker.

            DataTable dt = contenedorDTE.imprimirEstiquer(contenedorDTO, ordenDTO);//metodo que consulta el sp de los stiker que se generaron.
            foreach (DataRow row in dt.Rows)
            {
                Print(  row[1].ToString(), row[2].ToString(), row[3].ToString(), row[29].ToString(), 
                        row[4].ToString(), row[0].ToString(), row[46].ToString(), row[45].ToString(),
                        row[44].ToString(), row[43].ToString(), row[49].ToString(), row[39].ToString(),
                        row[10].ToString(), row[11].ToString(), row[7].ToString(), row[42].ToString());//Lamamos el metodo que imprime los stikers
            }
        }

        private void Print( String strUnitContainerType, String strProductCode, String strUnits, 
                            String strUnidMedida, String strCustomerProductCode, String strInventoryItemBarCode,
                            String strSubLneaEsHeatTranfer, String strPrecion, String strTiempo, String strTemperatura,
                            String strQuitarCdgoCliente, String strCdgpDisnoClnte, String strDocumentSource,
                            String strClientDBProductionOrderId, String strProductionOrdenId, String strOrdenCompra)
        {
            TSCEthernet IP = new TSCEthernet();
            
            try{

                IP.openport("192.21.21.35", 9100);
                IP.clearbuffer();
                IP.sendcommand("<xpml><page quantity='0' pitch='38.9 mm'></xpml>SIZE 78.7 mm, 38.9 mm\n");
                IP.sendcommand("DIRECTION 0,0\n");
                IP.sendcommand("REFERENCE 0,0\n");
                IP.sendcommand("OFFSET 0 mm\n");
                IP.sendcommand("SET PEEL OFF\n");
                IP.sendcommand("SET CUTTER OFF\n");
                IP.sendcommand("<xpml></page></xpml><xpml><page quantity='1' pitch='38.9 mm'></xpml>SET TEAR ON\n");
                IP.sendcommand("CLS\n");
                IP.sendcommand("CODEPAGE 1252\n");
                IP.sendcommand("TEXT 767,396,\"0\",180,8,8,\"" + strUnitContainerType + "\"\n");
                IP.sendcommand("TEXT 730,398,\"0\",180,8,8,\"" + strProductCode + "\"\n");
                IP.sendcommand("TEXT 428,396,\"0\",180,8,8,\"" + strUnits + " " + strUnidMedida + "\"\n");
                IP.sendcommand("TEXT 230,399,\"0\",180,8,8,\"" + strCustomerProductCode + "\"\n");

                //Inicio si la linea es heattransfer pintamos las tres lineas de codigo
                if (strSubLneaEsHeatTranfer.Equals("S"))
                {
                    IP.sendcommand("TEXT 766,349,\"3\",180,1,1,\"" + strPrecion + "\"\n");
                    IP.sendcommand("TEXT 767,316,\"3\",180,1,1,\"" + strTemperatura + "\"\n");
                    IP.sendcommand("TEXT 770,286,\"3\",180,1,1,\"" + strTiempo + "\"\n");
                }
                //Fin si la linea es heattransfer pintamos las tres lineas de codigo

                if (!strQuitarCdgoCliente.Equals("S"))
                {
                    IP.sendcommand("TEXT 759,199,\"0\",180,8,8,\"Lot\"\n");
                    IP.sendcommand("TEXT 757,170,\"0\",180,8,8,\"Cust.Ref\"\n");
                    IP.sendcommand("TEXT 758,134,\"0\",180,8,8,\"PO\"\n");
                    if (strDocumentSource.Equals(1))
                    {
                        IP.sendcommand("TEXT 708,196,\"3\",180,1,1,\"" + strClientDBProductionOrderId + "\"\n");
                    }
                    else
                    {
                        IP.sendcommand("TEXT 708,196,\"3\",180,1,1,\"" + strProductionOrdenId + "\"\n");
                    }

                    IP.sendcommand("TEXT 632,166,\"3\",180,1,1,\"" + strCdgpDisnoClnte + "\"\n");
                    IP.sendcommand("TEXT 709,133,\"3\",180,1,1,\"" + strOrdenCompra + "\"\n");
                }
                IP.sendcommand("TEXT 178,169,\"3\",180,1,1,\"Finotex\"\n");

                DateTime dt = DateTime.Now;
                DateTime utcdt = dt.ToUniversalTime();
                String format = "MM/dd/yyyy";

                IP.sendcommand("TEXT 225,133,\"3\",180,1,1,\"" + dt.ToString(format) + "\"\n");

                IP.sendcommand("PDF417 542,341,542,341,180,W4,H12,C3,T0,\"" + strInventoryItemBarCode + "\"\n");//Imprimirmo el codigo de barras

                IP.sendcommand("PRINT 1,1\n");
                IP.sendcommand("<xpml></page></xpml><xpml><end/></xpml>\n");

                IP.closeport();

            }catch(Exception)
            {
                IP.closeport();
            }
        }

        public void CalcularContenedoresEsperados()
        {
            try
            {
                Double _cntCntndorEsprda = Double.Parse(TXT_CNTIDAD_PRDCDA.Text) / Double.Parse(TXT_CNTIDAD_POR_CNTENDOR.Text);
                NO_CNTNDOR.Text = _cntCntndorEsprda.ToString();
                btnGenerarStiker.Enabled = true;
            }
            catch (Exception)
            { }
        }

        private bool ValidarCampos()
        {
            if (TXT_ORDER_ID.Text.Equals(""))
            {
                MessageBox.Show("Por favor escanee el codigo de la orden", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button3);
                return false;
            }
            return true;
        }

        private void TXT_ORDER_ID_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13 && ValidarCampos())
            {
                ConsultarOrdenProduccion();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            mtdGenerarEstiquer();
        }

        private void TXT_CNTIDAD_POR_CNTENDOR_Validated(object sender, EventArgs e)
        {
            CalcularContenedoresEsperados();
        }   
    }
}