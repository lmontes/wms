﻿namespace WMS.Vistas
{
    partial class FrmEmpImpsionEmpquePpal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlContenedor = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlBordeRojo = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cboClientes = new System.Windows.Forms.ComboBox();
            this.txtNmroOrden = new System.Windows.Forms.TextBox();
            this.rdbCliente = new System.Windows.Forms.RadioButton();
            this.rdbOrdenProdccion = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.txtCodigo = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pnlContenedor.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlContenedor
            // 
            this.pnlContenedor.BackColor = System.Drawing.Color.DimGray;
            this.pnlContenedor.Controls.Add(this.label1);
            this.pnlContenedor.Controls.Add(this.pnlBordeRojo);
            this.pnlContenedor.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlContenedor.Location = new System.Drawing.Point(0, 0);
            this.pnlContenedor.Name = "pnlContenedor";
            this.pnlContenedor.Size = new System.Drawing.Size(238, 26);
            // 
            // label1
            // 
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(3, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(208, 20);
            this.label1.Text = "Impresion empaque principal";
            // 
            // pnlBordeRojo
            // 
            this.pnlBordeRojo.BackColor = System.Drawing.Color.Brown;
            this.pnlBordeRojo.Location = new System.Drawing.Point(0, 24);
            this.pnlBordeRojo.Name = "pnlBordeRojo";
            this.pnlBordeRojo.Size = new System.Drawing.Size(238, 3);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.cboClientes);
            this.panel1.Controls.Add(this.txtNmroOrden);
            this.panel1.Controls.Add(this.rdbCliente);
            this.panel1.Controls.Add(this.rdbOrdenProdccion);
            this.panel1.Location = new System.Drawing.Point(3, 36);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(232, 92);
            // 
            // cboClientes
            // 
            this.cboClientes.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.cboClientes.Location = new System.Drawing.Point(22, 64);
            this.cboClientes.Name = "cboClientes";
            this.cboClientes.Size = new System.Drawing.Size(189, 19);
            this.cboClientes.TabIndex = 9;
            this.cboClientes.SelectedIndexChanged += new System.EventHandler(this.cboClientes_SelectedIndexChanged);
            this.cboClientes.SelectedValueChanged += new System.EventHandler(this.cboClientes_SelectedValueChanged);
            // 
            // txtNmroOrden
            // 
            this.txtNmroOrden.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.txtNmroOrden.Location = new System.Drawing.Point(22, 64);
            this.txtNmroOrden.Name = "txtNmroOrden";
            this.txtNmroOrden.Size = new System.Drawing.Size(189, 19);
            this.txtNmroOrden.TabIndex = 8;
            this.txtNmroOrden.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TXT_NMRO_ORDEN_KeyPress);
            // 
            // rdbCliente
            // 
            this.rdbCliente.Location = new System.Drawing.Point(12, 38);
            this.rdbCliente.Name = "rdbCliente";
            this.rdbCliente.Size = new System.Drawing.Size(155, 20);
            this.rdbCliente.TabIndex = 4;
            this.rdbCliente.TabStop = false;
            this.rdbCliente.Text = "Cliente";
            this.rdbCliente.CheckedChanged += new System.EventHandler(this.rdbCliente_CheckedChanged);
            // 
            // rdbOrdenProdccion
            // 
            this.rdbOrdenProdccion.Checked = true;
            this.rdbOrdenProdccion.Location = new System.Drawing.Point(12, 19);
            this.rdbOrdenProdccion.Name = "rdbOrdenProdccion";
            this.rdbOrdenProdccion.Size = new System.Drawing.Size(155, 20);
            this.rdbOrdenProdccion.TabIndex = 0;
            this.rdbOrdenProdccion.Text = "Orden de produccion";
            this.rdbOrdenProdccion.CheckedChanged += new System.EventHandler(this.rdbOrdenProdccion_CheckedChanged);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(5, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(152, 20);
            this.label2.Text = "Seleccione una opcion";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.button1.Location = new System.Drawing.Point(5, 241);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(228, 20);
            this.button1.TabIndex = 35;
            this.button1.Text = "Imprimir etiqueta principal";
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.txtNombre);
            this.panel2.Controls.Add(this.txtCodigo);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Location = new System.Drawing.Point(3, 138);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(232, 97);
            // 
            // txtNombre
            // 
            this.txtNombre.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.txtNombre.Location = new System.Drawing.Point(72, 50);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(148, 19);
            this.txtNombre.TabIndex = 8;
            // 
            // txtCodigo
            // 
            this.txtCodigo.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.txtCodigo.Location = new System.Drawing.Point(72, 30);
            this.txtCodigo.Name = "txtCodigo";
            this.txtCodigo.Size = new System.Drawing.Size(148, 19);
            this.txtCodigo.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(8, 49);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 20);
            this.label5.Text = "Nombre";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(8, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 20);
            this.label4.Text = "Codigo";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(1, -4);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(152, 20);
            this.label3.Text = "Informacion del cliente";
            // 
            // FrmEmpImpsionEmpquePpal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(238, 270);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pnlContenedor);
            this.Controls.Add(this.panel1);
            this.Name = "FrmEmpImpsionEmpquePpal";
            this.Text = "Finotex";
            this.pnlContenedor.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlContenedor;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel pnlBordeRojo;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton rdbOrdenProdccion;
        private System.Windows.Forms.RadioButton rdbCliente;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.TextBox txtCodigo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cboClientes;
        private System.Windows.Forms.TextBox txtNmroOrden;
    }
}