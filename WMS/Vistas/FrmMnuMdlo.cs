﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace WMS.Vistas
{
    public partial class FrmMnuMdlo : Form
    {
        public FrmMnuMdlo()
        {
            InitializeComponent();
            lblUsuario.Text = Globales.gs_nmbreUsuario;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FrmPrincipal frmPrincipal = new FrmPrincipal();
            frmPrincipal.ShowDialog();
        }

        private void FrmMnuMdlo_Load(object sender, EventArgs e)
        {

        }
    }
}