﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace WMS.Vistas
{
    public partial class FrmPrincipal : Form
    {
        public FrmPrincipal()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FrmMnuEmpque frmMnuPrcsoEmpque = new FrmMnuEmpque();
            frmMnuPrcsoEmpque.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FrmPreCut frmPreCut = new FrmPreCut();
            frmPreCut.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            FrmMnuInvntrio frmMnuInvntrio = new FrmMnuInvntrio();
            frmMnuInvntrio.ShowDialog();
        }
    }
}