﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WMS.DTE;
using WMS.DTO;

namespace WMS.Vistas
{
    public partial class FrmLogin : Form
    {
        Boolean _exists = false;
        String gs_userName = null;

        public FrmLogin()
        {
            InitializeComponent();
        }

        public bool userValidate()
        {
            UserDTO dto = new UserDTO();
            UserDTE dte = new UserDTE();

            try
            {
                if (txtLogin.Text.Equals(""))
                {
                    MessageBox.Show("Ingrese nombre de usuario", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                    return false;
                }

                if (txtPassword.Text.Equals(""))
                {
                    MessageBox.Show("Ingrese la contraseña", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                    return false;
                }

                //Byte[] strBytes = new UnicodeEncoding().GetBytes(txtPassword.Text.ToString().Trim());//Se encripta la contraseña del usuario
                //strBytes = new SHA1CryptoServiceProvider().ComputeHash(strBytes);

                dto._strLogin = txtLogin.Text.ToString();
                dto._strPassword = (txtPassword.Text);
                //dto = dte.userValidate(dto);

                Globales.gs_nmbreUsuario = dto._strNombre;

                if (dto._strLogin == null)
                {
                    MessageBox.Show("Credenciales invalidas", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                    return false;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Se presentó un error al validar el usuario", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                return false;
            }

            return true;
        }

        public Boolean getExists()
        {
            return _exists;
        }

        public String getUserName()
        {
            return gs_userName;
        }

        public void mtdListarBodegas()
        {
            BodegaDTO dto = new BodegaDTO();
            BodegaDTE dte = new BodegaDTE();

            try
            {
                dto._strLogin = txtLogin.Text.ToString();
                DataTable dt = dte.listarBodegasDelUsuario(dto);
                cbo_Bodegas.DataSource = dt;
                cbo_Bodegas.DisplayMember = "WarehouseDescription";
                cbo_Bodegas.ValueMember = "WarehouseCode";
            }
            catch (Exception e)
            {
                MessageBox.Show("Se presentó un error al listar las bodegas del usuario", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (userValidate().Equals(true))
            {
                txtLogin.Enabled = false;
                txtPassword.Enabled = false;
                btnValidar.Enabled = false;
                btnLimpiar.Enabled = false;
                btnCancel.Enabled = true;
                btnEntrar.Enabled = true;
                
                cbo_Bodegas.Enabled = true;
                mtdListarBodegas();
            }
            else
            {
                this._exists = false;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
            Globales.gs_cdgoBodega = cbo_Bodegas.SelectedValue.ToString();
            FrmMnuMdlo frmMnuMdlo = new FrmMnuMdlo();
            frmMnuMdlo.ShowDialog();

        }

        private void FrmLogin_Load(object sender, EventArgs e)
        {

        }
    }
}