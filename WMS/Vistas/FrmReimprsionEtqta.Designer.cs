﻿namespace WMS.Vistas
{
    partial class FrmReimprsionEtqta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlContenedor = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlBordeRojo = new System.Windows.Forms.Panel();
            this.cbo_TpoEtiqueta = new System.Windows.Forms.ComboBox();
            this.TXT_NMRO_CDGO = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pnlContenedor.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlContenedor
            // 
            this.pnlContenedor.BackColor = System.Drawing.Color.DimGray;
            this.pnlContenedor.Controls.Add(this.label1);
            this.pnlContenedor.Controls.Add(this.pnlBordeRojo);
            this.pnlContenedor.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlContenedor.Location = new System.Drawing.Point(0, 0);
            this.pnlContenedor.Name = "pnlContenedor";
            this.pnlContenedor.Size = new System.Drawing.Size(238, 26);
            // 
            // label1
            // 
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(3, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(208, 20);
            this.label1.Text = "Re-Impresión de etiquetas";
            // 
            // pnlBordeRojo
            // 
            this.pnlBordeRojo.BackColor = System.Drawing.Color.Brown;
            this.pnlBordeRojo.Location = new System.Drawing.Point(0, 24);
            this.pnlBordeRojo.Name = "pnlBordeRojo";
            this.pnlBordeRojo.Size = new System.Drawing.Size(238, 3);
            // 
            // cbo_TpoEtiqueta
            // 
            this.cbo_TpoEtiqueta.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.cbo_TpoEtiqueta.Location = new System.Drawing.Point(32, 125);
            this.cbo_TpoEtiqueta.Name = "cbo_TpoEtiqueta";
            this.cbo_TpoEtiqueta.Size = new System.Drawing.Size(189, 19);
            this.cbo_TpoEtiqueta.TabIndex = 7;
            // 
            // TXT_NMRO_CDGO
            // 
            this.TXT_NMRO_CDGO.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.TXT_NMRO_CDGO.Location = new System.Drawing.Point(32, 100);
            this.TXT_NMRO_CDGO.Name = "TXT_NMRO_CDGO";
            this.TXT_NMRO_CDGO.Size = new System.Drawing.Size(189, 19);
            this.TXT_NMRO_CDGO.TabIndex = 6;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(32, 150);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(189, 20);
            this.button1.TabIndex = 35;
            this.button1.Text = "Imprimir";
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(15, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(218, 20);
            this.label2.Text = "Escanee la localización, contenedor";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(15, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(212, 20);
            this.label3.Text = "principal o contenedor.";
            // 
            // FrmImpsionEmpquePpal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(238, 264);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cbo_TpoEtiqueta);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.TXT_NMRO_CDGO);
            this.Controls.Add(this.pnlContenedor);
            this.Name = "FrmImpsionEmpquePpal";
            this.Text = "Finotex";
            this.pnlContenedor.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlContenedor;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel pnlBordeRojo;
        private System.Windows.Forms.TextBox TXT_NMRO_CDGO;
        private System.Windows.Forms.ComboBox cbo_TpoEtiqueta;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}