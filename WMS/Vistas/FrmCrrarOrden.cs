﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Microsoft.Win32;
using MobilePrinter;
using MobilePrinter.TSCWinCE;
using WMS.DTE;
using WMS.DTO;

namespace WMS.Vistas
{
    public partial class FrmCrrarOrden : Form
    {
        public FrmCrrarOrden()
        {
            InitializeComponent();
        }

        public void imprimirEmpacar()
        {
            ContenedorDTO contenedorDTO = new ContenedorDTO();

            EmpaqueDTE empaqueDTE = new EmpaqueDTE();

            if (txtCntidad.Text.Equals(""))
            {
                MessageBox.Show("Ingrese un cantidad válida.", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                return;
            }

            Double ldbl_Cantidad = 0;
            ldbl_Cantidad = Double.Parse(txtCntidad.Text);
            if (ldbl_Cantidad <= 0 || ldbl_Cantidad > 99999)
            {
                MessageBox.Show("La cantidad no pede ser negativa, cero o mayo a 99999.", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                return;
            }

            contenedorDTO._strCdgoBarrasPDF417 = txtCodigoPDF417.Text;
            if (contenedorDTO._strCdgoBarrasPDF417.Length < 32 || contenedorDTO._strCdgoBarrasPDF417.Length > 32)
            {
                MessageBox.Show("Invalido código de barras de la unidad contenedora.", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                return;
            }

            if (!contenedorDTO._strCdgoBarrasPDF417.Substring(0, 1).Equals("S") &&
                !contenedorDTO._strCdgoBarrasPDF417.Substring(0, 1).Equals("R") &&
                !contenedorDTO._strCdgoBarrasPDF417.Substring(0, 1).Equals("C") &&
                !contenedorDTO._strCdgoBarrasPDF417.Substring(0, 1).Equals("O"))
            {
                MessageBox.Show("La unidad de contenedor es invalida.", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                return;
            }

            if (!txtEstdoOrden.Text.Equals("Open"))
            {
                MessageBox.Show("La orden de producción no esta abierta.", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                return;
            }

            contenedorDTO._dblCntdadProducida = Double.Parse(txtCntidad.Text.ToString());

            DataTable dt_StikerGenerados = empaqueDTE.EmpacarSobrante(contenedorDTO);

            foreach (DataRow row in dt_StikerGenerados.Rows)
            {
                Print(  row[1].ToString(), row[2].ToString(), row[3].ToString(), row[29].ToString(),
                        row[4].ToString(), row[0].ToString(), row[46].ToString(), row[45].ToString(),
                        row[44].ToString(), row[43].ToString(), row[49].ToString(), row[39].ToString(),
                        row[10].ToString(), row[11].ToString(), row[7].ToString(), row[42].ToString());//Lamamos el metodo que imprime los stikers
            }
        }

        private void Print( String strUnitContainerType, String strProductCode, String strUnits,
                            String strUnidMedida, String strCustomerProductCode, String strInventoryItemBarCode,
                            String strSubLneaEsHeatTranfer, String strPrecion, String strTiempo, String strTemperatura,
                            String strQuitarCdgoCliente, String strCdgpDisnoClnte, String strDocumentSource,
                            String strClientDBProductionOrderId, String strProductionOrdenId, String strOrdenCompra)
        {
            TSCEthernet IP = new TSCEthernet();
            IP.openport("192.21.21.35", 9100);
            IP.sendcommand("<xpml><page quantity='0' pitch='38.9 mm'></xpml>SIZE 78.7 mm, 38.9 mm\n");
            IP.sendcommand("DIRECTION 0,0\n");
            IP.sendcommand("REFERENCE 0,0\n");
            IP.sendcommand("OFFSET 0 mm\n");
            IP.sendcommand("SET PEEL OFF\n");
            IP.sendcommand("SET CUTTER OFF\n");
            IP.sendcommand("<xpml></page></xpml><xpml><page quantity='1' pitch='38.9 mm'></xpml>SET TEAR ON\n");
            IP.sendcommand("CLS\n");
            IP.sendcommand("CODEPAGE 1252\n");
            IP.sendcommand("TEXT 767,396,\"0\",180,8,8,\"" + strUnitContainerType + "\"\n");
            IP.sendcommand("TEXT 730,398,\"0\",180,8,8,\"" + strProductCode + "\"\n");
            IP.sendcommand("TEXT 428,396,\"0\",180,8,8,\"" + strUnits + " " + strUnidMedida + "\"\n");
            IP.sendcommand("TEXT 230,399,\"0\",180,8,8,\"" + strCustomerProductCode + "\"\n");

            //Inicio si la linea es heattransfer pintamos las tres lineas de codigo
            if (strSubLneaEsHeatTranfer.Equals("S"))
            {
                IP.sendcommand("TEXT 766,349,\"3\",180,1,1,\"" + strPrecion + "\"\n");
                IP.sendcommand("TEXT 767,316,\"3\",180,1,1,\"" + strTemperatura + "\"\n");
                IP.sendcommand("TEXT 770,286,\"3\",180,1,1,\"" + strTiempo + "\"\n");
            }
            //Fin si la linea es heattransfer pintamos las tres lineas de codigo

            if (!strQuitarCdgoCliente.Equals("S"))
            {
                IP.sendcommand("TEXT 759,199,\"0\",180,8,8,\"Lot\"\n");
                IP.sendcommand("TEXT 757,170,\"0\",180,8,8,\"Cust.Ref\"\n");
                IP.sendcommand("TEXT 758,134,\"0\",180,8,8,\"PO\"\n");
                if (strDocumentSource.Equals(1))
                {
                    IP.sendcommand("TEXT 708,196,\"3\",180,1,1,\"" + strClientDBProductionOrderId + "\"\n");
                }
                else
                {
                    IP.sendcommand("TEXT 708,196,\"3\",180,1,1,\"" + strProductionOrdenId + "\"\n");
                }

                IP.sendcommand("TEXT 632,166,\"3\",180,1,1,\"" + strCdgpDisnoClnte + "\"\n");
                IP.sendcommand("TEXT 709,133,\"3\",180,1,1,\"" + strOrdenCompra + "\"\n");
            }
            IP.sendcommand("TEXT 178,169,\"3\",180,1,1,\"Finotex\"\n");

            DateTime dt = DateTime.Now;
            DateTime utcdt = dt.ToUniversalTime();
            String format = "MM/dd/yyyy";

            IP.sendcommand("TEXT 225,133,\"3\",180,1,1,\"" + dt.ToString(format) + "\"\n");

            IP.sendcommand("PDF417 542,341,542,341,180,W4,H12,C3,T0,\"" + strInventoryItemBarCode + "\"\n");//Imprimirmo el codigo de barras

            IP.sendcommand("PRINT 1,1\n");
            IP.sendcommand("<xpml></page></xpml><xpml><end/></xpml>\n");

            IP.closeport();
        }

        public void ConsultarOrdenDeProduccionPorCodigoContenedor()
        {
            ContenedorDTO contenedorDTO = new ContenedorDTO();
            OrdenDTO ordenDTO = new OrdenDTO();

            EmpaqueDTE empaqueDTE = new EmpaqueDTE();
            OrdenDTE ordenDTE = new OrdenDTE();

            String lsCdigo = txtCodigoPDF417.Text;

            if (lsCdigo.Length < 32)
            {
                MessageBox.Show("La unidad de contenedor es invalida.", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                txtCodigoPDF417.Focus();//Paramos el codigo nuevamente en el campo que lee el codigo de barras.
                txtCodigoPDF417.SelectAll();
                return;
            }

            contenedorDTO._strCdgoBarrasPDF417 = txtCodigoPDF417.Text.Trim();
            ordenDTO = ordenDTE.ObtenerIdEstadoDeLaOrdenPorUnitContainer(contenedorDTO);

            if (ordenDTO._strFuente == null)
            {
                MessageBox.Show("No existe información para la unidad de contenedor.", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
            }
            else
            {
                txtClientOrdeProduccion.Text = ordenDTO._strClientDBProductionOrderId.ToString();
                txtIdOrdeProduccion.Text = ordenDTO._strProductionOrderId.ToString();
                txtEstdoOrden.Text = ordenDTO._strEstado.ToString();
            }
        }

        private void txtCodigoPDF417_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                ConsultarOrdenDeProduccionPorCodigoContenedor();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            imprimirEmpacar();
        }
    }
}