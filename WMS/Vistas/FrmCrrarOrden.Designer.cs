﻿namespace WMS.Vistas
{
    partial class FrmCrrarOrden
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlContenedor = new System.Windows.Forms.Panel();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.pnlBordeRojo = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.txtCodigoPDF417 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCntidad = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.txtEstdoOrden = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.txtClientOrdeProduccion = new System.Windows.Forms.TextBox();
            this.txtIdOrdeProduccion = new System.Windows.Forms.TextBox();
            this.pnlContenedor.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlContenedor
            // 
            this.pnlContenedor.BackColor = System.Drawing.Color.DimGray;
            this.pnlContenedor.Controls.Add(this.lblTitulo);
            this.pnlContenedor.Controls.Add(this.pnlBordeRojo);
            this.pnlContenedor.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlContenedor.Location = new System.Drawing.Point(0, 0);
            this.pnlContenedor.Name = "pnlContenedor";
            this.pnlContenedor.Size = new System.Drawing.Size(238, 26);
            // 
            // lblTitulo
            // 
            this.lblTitulo.ForeColor = System.Drawing.Color.White;
            this.lblTitulo.Location = new System.Drawing.Point(3, 3);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(208, 20);
            this.lblTitulo.Text = "Cerrar e imprimir Orden";
            // 
            // pnlBordeRojo
            // 
            this.pnlBordeRojo.BackColor = System.Drawing.Color.Brown;
            this.pnlBordeRojo.Location = new System.Drawing.Point(0, 24);
            this.pnlBordeRojo.Name = "pnlBordeRojo";
            this.pnlBordeRojo.Size = new System.Drawing.Size(238, 3);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(6, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(201, 16);
            this.label1.Text = "Escanea una unidad de contenedor";
            // 
            // txtCodigoPDF417
            // 
            this.txtCodigoPDF417.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtCodigoPDF417.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.txtCodigoPDF417.ForeColor = System.Drawing.Color.Black;
            this.txtCodigoPDF417.Location = new System.Drawing.Point(6, 43);
            this.txtCodigoPDF417.Name = "txtCodigoPDF417";
            this.txtCodigoPDF417.Size = new System.Drawing.Size(227, 19);
            this.txtCodigoPDF417.TabIndex = 27;
            this.txtCodigoPDF417.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodigoPDF417_KeyPress);
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label3.Location = new System.Drawing.Point(6, 64);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(115, 16);
            this.label3.Text = "Orden de P. del cliente";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label2.Location = new System.Drawing.Point(6, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 16);
            this.label2.Text = "Orden WMS";
            // 
            // txtCntidad
            // 
            this.txtCntidad.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular);
            this.txtCntidad.ForeColor = System.Drawing.Color.Red;
            this.txtCntidad.Location = new System.Drawing.Point(127, 104);
            this.txtCntidad.Name = "txtCntidad";
            this.txtCntidad.Size = new System.Drawing.Size(105, 18);
            this.txtCntidad.TabIndex = 90;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label4.Location = new System.Drawing.Point(6, 100);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(115, 16);
            this.label4.Text = "Cantidad";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.button1.Location = new System.Drawing.Point(33, 128);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(172, 20);
            this.button1.TabIndex = 93;
            this.button1.Text = "Imprimir y empacar";
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(2, 163);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(201, 16);
            this.label5.Text = "Cerrar orden de producción";
            // 
            // txtEstdoOrden
            // 
            this.txtEstdoOrden.BackColor = System.Drawing.Color.Silver;
            this.txtEstdoOrden.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.txtEstdoOrden.Location = new System.Drawing.Point(6, 203);
            this.txtEstdoOrden.Name = "txtEstdoOrden";
            this.txtEstdoOrden.ReadOnly = true;
            this.txtEstdoOrden.Size = new System.Drawing.Size(227, 19);
            this.txtEstdoOrden.TabIndex = 96;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label6.Location = new System.Drawing.Point(6, 190);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(226, 16);
            this.label6.Text = "Estado actual de la orden de producción:";
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.button2.Location = new System.Drawing.Point(33, 231);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(172, 20);
            this.button2.TabIndex = 99;
            this.button2.Text = "Cerrar ordend e producción";
            // 
            // txtClientOrdeProduccion
            // 
            this.txtClientOrdeProduccion.BackColor = System.Drawing.Color.Silver;
            this.txtClientOrdeProduccion.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.txtClientOrdeProduccion.Location = new System.Drawing.Point(127, 64);
            this.txtClientOrdeProduccion.Name = "txtClientOrdeProduccion";
            this.txtClientOrdeProduccion.ReadOnly = true;
            this.txtClientOrdeProduccion.Size = new System.Drawing.Size(105, 19);
            this.txtClientOrdeProduccion.TabIndex = 100;
            // 
            // txtIdOrdeProduccion
            // 
            this.txtIdOrdeProduccion.BackColor = System.Drawing.Color.Silver;
            this.txtIdOrdeProduccion.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.txtIdOrdeProduccion.Location = new System.Drawing.Point(127, 84);
            this.txtIdOrdeProduccion.Name = "txtIdOrdeProduccion";
            this.txtIdOrdeProduccion.ReadOnly = true;
            this.txtIdOrdeProduccion.Size = new System.Drawing.Size(105, 19);
            this.txtIdOrdeProduccion.TabIndex = 101;
            // 
            // FrmCrrarOrden
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(238, 270);
            this.Controls.Add(this.txtIdOrdeProduccion);
            this.Controls.Add(this.txtClientOrdeProduccion);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.txtEstdoOrden);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtCntidad);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtCodigoPDF417);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pnlContenedor);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label6);
            this.Name = "FrmCrrarOrden";
            this.Text = "Producto terminado";
            this.pnlContenedor.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlContenedor;
        private System.Windows.Forms.Label lblTitulo;
        private System.Windows.Forms.Panel pnlBordeRojo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtCodigoPDF417;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtCntidad;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtEstdoOrden;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox txtClientOrdeProduccion;
        private System.Windows.Forms.TextBox txtIdOrdeProduccion;
    }
}