﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WMS.DTE;
using WMS.DTO;

namespace WMS.Vistas
{
    public partial class FrmDvdirCntndoEmpque : Form
    {
        DataTable dt;
        Double li_Cntidad;

        public FrmDvdirCntndoEmpque()
        {
            InitializeComponent();

            li_Cntidad = 0;

            DataGridTableStyle tabStyle = new DataGridTableStyle();
            tabStyle.MappingName = "Person";

            DataGridTextBoxColumn col1 = new DataGridTextBoxColumn();
            col1.MappingName = "name";
            col1.HeaderText = "Unidades Contenidas";
            col1.Width = 225;

            tabStyle.GridColumnStyles.Add(col1);

            this.DG_CONTENEDOR.TableStyles.Clear();
            this.DG_CONTENEDOR.TableStyles.Add(tabStyle);

            dt = new DataTable("Person");
            dt.Columns.Add("name");
            this.DG_CONTENEDOR.DataSource = dt;

        }

        public Boolean mtdValidarSleevIngresado()
        {
            foreach (DataRow row in dt.Rows)
            {
                if (row[0].ToString().Equals(txtCntndorAntiguo.Text))
                {
                    MessageBox.Show("El contenedor " + row[0].ToString() + " ya fue leido.", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                    txtCntndorAntiguo.Text = "";
                    txtCntndorAntiguo.Focus();
                    return false;
                }
            }

            return true;
        }

        public void leerCodigoAntiguo()
        {
            ContenedorDTO contenedorDTO = new ContenedorDTO();
            ArticuloDTO articuloDTO = new ArticuloDTO();


            OrdenDTE ordenDTE = new OrdenDTE();
            //ContenedorDTE contenedorDTE = new ContenedorDTE();
            

            String lsCdigo = txtCntndorAntiguo.Text;

            if (!(lsCdigo.Substring(0, 1).Equals("S") ||
                lsCdigo.Substring(0, 1).Equals("R") ||
                lsCdigo.Substring(0, 1).Equals("C") ||
                lsCdigo.Substring(0, 1).Equals("O")))
            {
                MessageBox.Show("La unidad de contenedor es invalida.", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                txtCntndorAntiguo.Focus();//Paramos el codigo nuevamente en el campo que lee el codigo de barras.
                txtCntndorAntiguo.SelectAll();
                return;
            }

            if (lsCdigo.Length < 32)
            {
                MessageBox.Show("La unidad de contenedor es invalida.", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                txtCntndorAntiguo.Focus();//Paramos el codigo nuevamente en el campo que lee el codigo de barras.
                txtCntndorAntiguo.SelectAll();
                return;
            }

            contenedorDTO._strCdgoBarrasPDF417=txtCntndorAntiguo.Text.Trim();
            articuloDTO = ordenDTE.ObtenerInformacionDeLaOrdenPorUnitContainer(contenedorDTO);

            if (articuloDTO._strFuente == null)
            {
                MessageBox.Show("No existe información para la unidad de contenedor.", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
            }
            else
            {
                if (articuloDTO._strFuente.Equals("1"))
                {
                    txtOrdenPrdccion.Text = articuloDTO._strClientDBProductionOrderId.ToString();
                }
                if (articuloDTO._strFuente.Equals("2"))
                {
                    txtOrdenPrdccion.Text = articuloDTO._strProductionOrderId.ToString();
                }
                txtCdgoPrdcto.Text = articuloDTO._lngCdgoArtclo.ToString();
                txtEstdoOrden.Text = articuloDTO._strEstado.ToString();
                txtCntdad.Text = articuloDTO._dblCntdadProducida.ToString();
                txtCntndorNuevo.Focus();//Paramos el codigo nuevamente en el campo que lee el codigo de barras.
                txtCntndorNuevo.SelectAll();
            }
        }

        public Boolean validarUnidadContenedoraIngresada()
        {
            foreach (DataRow row in dt.Rows)
            {
                if (row[0].ToString().Equals(txtCntndorNuevo.Text))
                {
                    MessageBox.Show("La unidad contenedora " + row[0].ToString() + " ya fue leida.", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                    txtCntndorNuevo.Text = "";
                    txtCntndorNuevo.Focus();
                    return false;
                }
            }

            return true;
        }

        public void leerCodigoNuevo()
        {
            OrdenDTE ordenDTE = new OrdenDTE();
            OrdenDTO ordenDTO = new OrdenDTO();

            ContenedorDTE contenedorDTE = new ContenedorDTE();
            IdentificadorDTO contenedorDTO = new IdentificadorDTO();

            if (txtCntndorNuevo.Text.Trim().Equals(""))
            {
                MessageBox.Show("Lea la unidad contenedora antigua inicialmente.", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                txtCntndorAntiguo.Focus();//Paramos el codigo nuevamente en el campo que lee el codigo de barras.
                txtCntndorAntiguo.SelectAll();
                return;
            }

            String lsCdigo = txtCntndorNuevo.Text;

            if (lsCdigo.Length != 32)
            {
                MessageBox.Show("El código de la unidad contenedor es invalida.", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                txtCntndorNuevo.Focus();//Paramos el codigo nuevamente en el campo que lee el codigo de barras.
                txtCntndorNuevo.SelectAll();
                return;
            }

            if (!(lsCdigo.Substring(0, 1).Equals("S") ||
                lsCdigo.Substring(0, 1).Equals("R") ||
                lsCdigo.Substring(0, 1).Equals("C") ||
                lsCdigo.Substring(0, 1).Equals("O")))
            {
                MessageBox.Show("El código de la unidad contenedor es invalida.", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                txtCntndorNuevo.Focus();//Paramos el codigo nuevamente en el campo que lee el codigo de barras.
                txtCntndorNuevo.SelectAll();
                return;
            }
            
            if (lsCdigo.Equals(txtCntndorAntiguo.Text))//Verificamos que el codigo de barras nuevo no sea igual al codigo de barra antiguo.
            {
                MessageBox.Show("La uniad contenedora no puede ser igual a la antigua", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                txtCntndorAntiguo.Focus();//Paramos el codigo nuevamente en el campo que lee el codigo de barras.
                txtCntndorAntiguo.SelectAll();
                return;
            }

            
            dt.Rows.Add(txtCntndorNuevo.Text);
            Int32 li_fila = DG_CONTENEDOR.CurrentRowIndex;
            CalculaCantidad("+", li_fila);

            LBL_MENSAJE.Text = (dt.Rows.Count + 1).ToString() + " cajitas agregadas";
            btnDividir.Enabled = true;
            btnLimpiar.Enabled = true;
            txtCntndorNuevo.Text = "";
            txtCntndorNuevo.Focus();
            return;
        }

        public void Guardar()
        {
            EmpaqueDTE empaqueDTE = new EmpaqueDTE();

            if (txtCntndorAntiguo.Text.Trim().Equals(""))
            {
                MessageBox.Show("Lea la unidad contenedora antigua inicialmente.", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                txtCntndorAntiguo.Focus();//Paramos el codigo nuevamente en el campo que lee el codigo de barras.
                txtCntndorAntiguo.SelectAll();
                return;
            }

            if (int.Parse(txtCntdad.Text) <= 0)
            {
                MessageBox.Show("Lea una unidad contenedora valida.", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                txtCntndorAntiguo.Focus();//Paramos el codigo nuevamente en el campo que lee el codigo de barras.
                txtCntndorAntiguo.SelectAll();
                return;
            }

            if (dt.Rows.Count <= 0)
            {
                MessageBox.Show("lea la nueva unidad contenedora", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                txtCntndorAntiguo.Focus();//Paramos el codigo nuevamente en el campo que lee el codigo de barras.
                txtCntndorAntiguo.SelectAll();
                return;
            }

            if (dt.Rows.Count == 0)
            {
                MessageBox.Show("Debe tener por lo menos dos unidades contenedoras", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                txtCntndorAntiguo.Focus();//Paramos el codigo nuevamente en el campo que lee el codigo de barras.
                txtCntndorAntiguo.SelectAll();
                return;
            }

            if (li_Cntidad != Int32.Parse(txtCntdad.Text))
            {
                MessageBox.Show("Error en las cantidades a dividir", "Finotex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
            }

            String sCntndoEmpque = dt.Rows.Count + "|";
            foreach (DataRow row in dt.Rows)
            {
                sCntndoEmpque = sCntndoEmpque + row[0].ToString();
            }

            empaqueDTE.DividirEmpaque(txtCntndorAntiguo.Text, sCntndoEmpque);
        }

        public void CalculaCantidad(String ls_simbolo, Int32 li_fila)
        {
            object selectedItem = DG_CONTENEDOR[li_fila, 0];
            Double ldble_cntdadActual = Double.Parse((Convert.ToString(selectedItem)).Substring(16, 9));

            switch (ls_simbolo)
            {
                case "+":
                    li_Cntidad = li_Cntidad + ldble_cntdadActual;
                    break;
                case "-":
                    li_Cntidad = li_Cntidad - ldble_cntdadActual;
                    dt.Rows.RemoveAt(li_fila);
                    break;
            }            
        }

        public void Reset()
        {
            dt.Rows.Clear();
            txtCntndorAntiguo.Text = "";
            txtOrdenPrdccion.Text = "";
            txtCdgoPrdcto.Text = "";
            txtEstdoOrden.Text = "";
            txtCntdad.Text = "";
            txtCntndorNuevo.Text = "";
            li_Cntidad = 0;
        }

        private void txtCdgobrras_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                leerCodigoAntiguo();
            }
        }

        private void textBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (validarUnidadContenedoraIngresada())
                {
                    leerCodigoNuevo();
                }
            }
        }

        private void btnDividir_Click(object sender, EventArgs e)
        {
            Guardar();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Int32 li_fila = DG_CONTENEDOR.CurrentRowIndex;
            if (li_fila >= 0)
            {
                CalculaCantidad("-",li_fila);
            }
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            Reset();
        }
    }
}