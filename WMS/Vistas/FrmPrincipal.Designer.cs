﻿namespace WMS.Vistas
{
    partial class FrmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.pnlPrincipal = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.pnlContenedor = new System.Windows.Forms.Panel();
            this.pnlBordeRojo = new System.Windows.Forms.Panel();
            this.lblUsuario = new System.Windows.Forms.Label();
            this.pnlPrincipal.SuspendLayout();
            this.pnlContenedor.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(14, 90);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(210, 26);
            this.button1.TabIndex = 0;
            this.button1.Text = "Proceso de empaque";
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // pnlPrincipal
            // 
            this.pnlPrincipal.Controls.Add(this.button2);
            this.pnlPrincipal.Controls.Add(this.button5);
            this.pnlPrincipal.Controls.Add(this.button4);
            this.pnlPrincipal.Controls.Add(this.button3);
            this.pnlPrincipal.Controls.Add(this.button1);
            this.pnlPrincipal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlPrincipal.Location = new System.Drawing.Point(0, 0);
            this.pnlPrincipal.Name = "pnlPrincipal";
            this.pnlPrincipal.Size = new System.Drawing.Size(237, 270);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(14, 63);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(210, 26);
            this.button2.TabIndex = 6;
            this.button2.Text = "Generacion de estiquer";
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(14, 168);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(210, 26);
            this.button5.TabIndex = 4;
            this.button5.Text = "Administration/Maintenenace";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(14, 142);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(210, 26);
            this.button4.TabIndex = 3;
            this.button4.Text = "Picking for Shipment/Unpicking";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(14, 116);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(210, 26);
            this.button3.TabIndex = 2;
            this.button3.Text = "Inventario";
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // pnlContenedor
            // 
            this.pnlContenedor.BackColor = System.Drawing.Color.DimGray;
            this.pnlContenedor.Controls.Add(this.pnlBordeRojo);
            this.pnlContenedor.Controls.Add(this.lblUsuario);
            this.pnlContenedor.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlContenedor.Location = new System.Drawing.Point(0, 0);
            this.pnlContenedor.Name = "pnlContenedor";
            this.pnlContenedor.Size = new System.Drawing.Size(237, 26);
            // 
            // pnlBordeRojo
            // 
            this.pnlBordeRojo.BackColor = System.Drawing.Color.Brown;
            this.pnlBordeRojo.Location = new System.Drawing.Point(0, 24);
            this.pnlBordeRojo.Name = "pnlBordeRojo";
            this.pnlBordeRojo.Size = new System.Drawing.Size(238, 3);
            // 
            // lblUsuario
            // 
            this.lblUsuario.ForeColor = System.Drawing.Color.White;
            this.lblUsuario.Location = new System.Drawing.Point(3, 5);
            this.lblUsuario.Name = "lblUsuario";
            this.lblUsuario.Size = new System.Drawing.Size(143, 20);
            this.lblUsuario.Text = "Producto terminado";
            // 
            // FrmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(237, 270);
            this.Controls.Add(this.pnlContenedor);
            this.Controls.Add(this.pnlPrincipal);
            this.Name = "FrmPrincipal";
            this.Text = "Finotex";
            this.pnlPrincipal.ResumeLayout(false);
            this.pnlContenedor.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel pnlPrincipal;
        private System.Windows.Forms.Panel pnlContenedor;
        private System.Windows.Forms.Label lblUsuario;
        private System.Windows.Forms.Panel pnlBordeRojo;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
    }
}