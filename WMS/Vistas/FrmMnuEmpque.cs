﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace WMS.Vistas
{
    public partial class FrmMnuEmpque : Form
    {
        public FrmMnuEmpque()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            FrmEmpque frmEmpque = new FrmEmpque("E");
            frmEmpque.ShowDialog();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            FrmEmpImpsionEmpquePpal frmImpsionEmpquePpal = new FrmEmpImpsionEmpquePpal();//El parametro esta en E organiza la ventana para hacer proceso de empacar.
            frmImpsionEmpquePpal.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            FrmEmpque frmEmpque = new FrmEmpque("M");
            frmEmpque.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            FrmDvdirCntndoEmpque frmDvdirCntndoEmpque = new FrmDvdirCntndoEmpque();
            frmDvdirCntndoEmpque.ShowDialog();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            FrmDvdirCntndor frmDvdirCntndor = new FrmDvdirCntndor();
            frmDvdirCntndor.ShowDialog();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            FrmCrrarOrden frmCrrarOrden = new FrmCrrarOrden();
            frmCrrarOrden.ShowDialog();

        }
    }
}