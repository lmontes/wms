﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace WMS.DTO
{
    class IdentificadorDTO : OrdenDTO
    {
        //public Double _strCantidad{ get; set; }
        public String _strCdgoBarrasPDF417 { get; set; }
        public DateTime _dtmFecha { get; set; }
        //public Double _intUnidadesContenidas { get; set; }
        //public String _strNoOrdenProduccion { get; set; }
        //public String _strBatchId { get; set; }
        //public DateTime _dtmFecha { get; set; }
        
    }
}
