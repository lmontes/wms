﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace WMS.DTO
{
    class ContenedorDTO : IdentificadorDTO
    {
        public String _strTipo { get; set; }//El tipo de contenedor, puede ser un rollo, un sleev, una cajita u otro.
        public Double _dblPeso { get; set; }
        public String _strBatchId { get; set; }
        public Double _dblCantContenidas { get; set; }//Es la cantidad que se encuentra contenida dentro del contenedor
    }
}

