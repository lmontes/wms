﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace WMS.DTO
{
    class ClienteDTO
    {
        public String _strCodigo { get; set; }//Codigo del cliente
        public String _strRzonScial { get; set; }//Nombre del cliente
    }
}
