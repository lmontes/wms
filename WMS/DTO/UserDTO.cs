﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace WMS.DTO
{
    class UserDTO
    {
        public Int32 _intId { get; set; }
        public String _strLogin { get; set; }
        public String _strPassword { get; set; }
        public String _strNombre { get; set; }
        public String _strApellidos { get; set; }
    }
}
