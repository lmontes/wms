﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace WMS.DTO
{
    class ArticuloDTO : OrdenDTO
    {
        public String _lngCdgoArtclo { get; set; }
        public String _strNmbreArtclo { get; set; }
        public String _strAncho { get; set; }
        public String _strLrgo { get; set; }
        public String _strClor { get; set; }
        public String _strNmbreSubLneaInv { get; set; }
        public String _strNmbreCldad { get; set; } 
        //public String _strCntdadPrdcda { get; set; }
        //public String _strOrdenWms { get; set; }
    }
}
