﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace WMS.DTO
{
    class OrdenDTO
    {
        public String _strProductionOrderId { get; set; }//Es la orden de produccion de wms.
        public String _strClientDBProductionOrderId { get; set; }//Es la orden de producción de power.
        public String _strClientDBSaleOrderId { get; set; }//Es la orden de pedido.
        public String _strOrigen { get; set; }//Indica si el origen de la orden es de inventario o de produccion.
        public String _strFuente { get; set; }//Indica si el codigo fuente de de una orden de produccion o de wms.
        public String _strEstado { get; set; }//Parametro el cual puede ser el numero de la orden de produccion de un cliente o un id de orden de producción.
        public Double _dblCntdadProducida { get; set; }//Cantidad producida en una orden de producción.       
        public Double _dblCntdadRed { get; set; }//Cantidad producida en una orden de producción.       
    }
}
