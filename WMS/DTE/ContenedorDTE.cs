﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using WMS.DAO;
using WMS.DTO;

namespace WMS.DTE
{
    class ContenedorDTE
    {
        
        private SqlConnection conn;
        private ContenedorDAO dao;
        private ConexionBD_DTE BD;

        public ContenedorDTE()
        {

            BD = new ConexionBD_DTE("WMS");
            conn = BD.Conn;
            dao = new ContenedorDAO();
        }

        public ContenedorDTO generarEstiquer(ContenedorDTO contenedorDTO, OrdenDTO ordenDTO, Double ld_Cant)
        {
            return dao.generarEstiquer(conn, contenedorDTO, ordenDTO, ld_Cant);
        }

        public DataTable imprimirEstiquer(ContenedorDTO contenedorDTO, OrdenDTO ordenDTO)
        {
            return dao.imprimirEstiquer(conn, contenedorDTO, ordenDTO);
        }

        public ContenedorDTO GenerarContenedorPrincipal()
        {
            return dao.GenerarContenedorPrincipal(conn);
        }
    }
}
