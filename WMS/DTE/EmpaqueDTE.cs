﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using WMS.DAO;
using WMS.DTO;
namespace WMS.DTE
{
    class EmpaqueDTE
    {
        private SqlConnection conn;
        private EmpaqueDAO dao;
        private ConexionBD_DTE BD;

        public EmpaqueDTE()
        {
            BD = new ConexionBD_DTE("WMS");
            conn = BD.Conn;
            dao = new EmpaqueDAO();
        }

        public void Empacar(ContenedorDTO contenedorDTO, String ls_loclzcion, String ls_cntndor, String ls_listaCntndres)
        {
            dao.Empacar(conn, contenedorDTO, ls_loclzcion, ls_cntndor, ls_listaCntndres);
        }

        public void Desempacar(ContenedorDTO contenedorDTO, String ls_loclzcion, String ls_cntndor, String ls_listaCntndres)
        {
            dao.Desempacar(conn, contenedorDTO, ls_loclzcion, ls_cntndor, ls_listaCntndres);
        }

        public void Mover(ContenedorDTO contenedorDTO, String ls_loclzcion, String ls_cntndor, String ls_listaCntndres)
        {
            dao.Mover(conn, contenedorDTO, ls_loclzcion, ls_cntndor, ls_listaCntndres);
        }

        public void DividirEmpaque(String ls_cntndor, String ls_listaCntndres)
        {
            dao.DividirEmpaque(conn, ls_cntndor, ls_listaCntndres);
        }

        public DataTable DividirContenedor(String ls_listaCntndres)
        {
            return dao.DividirContenedor(conn, ls_listaCntndres);
        }

        public DataTable EmpacarSobrante(ContenedorDTO contenedorDTO)
        {
            return dao.EmpacarSobrante(conn, contenedorDTO);
        }
    }
}
