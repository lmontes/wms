﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using WMS.DAO;
using WMS.DTO;

namespace WMS.DTE
{
    class OrdenDTE
    {
        
        private SqlConnection conn;
        private OrdenDAO dao;
        private ConexionBD_DTE BD;

        public OrdenDTE()
        {
            BD = new ConexionBD_DTE("WMS");
            conn = BD.Conn;
            dao = new OrdenDAO();
        }

        public ArticuloDTO ConsultarOrden(OrdenDTO dto)
        {
            return dao.ConsultarOrden(conn, dto);
        }
        public ClienteDTO RecuperarCliente(String ls_Origen, String ls_Parametro)
        {
            return dao.RecuperarCliente(conn, ls_Origen, ls_Parametro);
        }
        public ArticuloDTO ObtenerInformacionDeLaOrdenPorUnitContainer(ContenedorDTO dto)
        {
            return dao.ObtenerInformacionDeLaOrdenPorUnitContainer(conn, dto);
        }
        public OrdenDTO ObtenerIdEstadoDeLaOrdenPorUnitContainer(ContenedorDTO dto)
        {
            return dao.ObtenerIdEstadoDeLaOrdenPorUnitContainer(conn, dto);
        }
    }
}
