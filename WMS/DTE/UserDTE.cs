﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using WMS.DAO;
using WMS.DTO;

namespace WMS.DTE
{
    class UserDTE
    {
        String _strConn;
        private SqlConnection conn;
        private UserDAO dao;
        private ConexionBD_DTE BD;

        public UserDTE()
        {
            
            BD = new ConexionBD_DTE("pf_colombia");
            conn = BD.Conn;
            dao = new UserDAO();
        }

        public UserDTO userValidate(UserDTO dto)
        {
            return dao.oper_UserValidate(conn, dto);
        }
    }
}
