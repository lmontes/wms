﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using WMS.DAO;
using WMS.DTO;

namespace WMS.DTE
{
    class BodegaDTE
    {
        private SqlConnection conn;
        private BodegaDAO dao;
        private ConexionBD_DTE BD;

        public BodegaDTE()
        {
            BD = new ConexionBD_DTE("WMS");
            conn = BD.Conn;
            dao = new BodegaDAO();
            
        }

        public DataTable listarBodegasDelUsuario(BodegaDTO dto)
        {
            return dao.listarBodegasDelUsuario(conn, dto);
        }

        public BodegaDTO ConsultarBodegaPorLocalizacion(LocalizacionDTO dto)
        {
            return dao.ConsultarBodegaPorLocalizacion(conn, dto);
        }
    }
}
