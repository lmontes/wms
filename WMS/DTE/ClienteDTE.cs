﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using WMS.DAO;
using WMS.DTO;

namespace WMS.DTE
{
    class ClienteDTE
    {
        
        private SqlConnection conn;
        private ClienteDAO dao;
        private ConexionBD_DTE BD;
        //Lester 


        public ClienteDTE()
        {
            
            BD = new ConexionBD_DTE("WMS");
            conn = BD.Conn;
            dao = new ClienteDAO();
        }

        public DataTable listarClientes()
        {
            return dao.listarClientes(conn);
        }
    }
}
